import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { GetService } from '../../services/get.service';
import { PostService } from '../../services/post.service';
import { Contact } from '../../contact';
import { debuglog } from 'util';
import { NgxSpinnerService } from 'ngx-spinner';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { RouterModule, Router } from '@angular/router';
import { AppComponent } from '../../app.component';






@Component({
  selector: 'app-phone-directory',
  templateUrl: './phone-directory.component.html',
  styleUrls: ['./phone-directory.component.css']
})
export class PhoneDirectoryComponent implements OnInit {

  @ViewChild('contactForm') form: any;
  public contact = new Contact;
  // submitted: boolean = false;
  public contactList: any = [];
  public updateModal: any;
  public empidExists: boolean = false;
  public empmessage: any;
  myModel: any;
  public mobliliList: any = [];
  public contactNumber: any = {};
  public AllContact: boolean = false;
  public AddContact: boolean = false;
  public successField: any = false;
  public successEdit: boolean;
  public authenticationError: boolean;
  public noContactCreated: boolean;
  public user: any = {};
  public loginId: any = {};
  public deleteField: boolean;
  public locationList: any[];
  public departmentList: any[];
  public department: any = {};
  public locationName: any;
  public bdayCheck: boolean;
  public anniversarySMSCheck: boolean;
  public contactExists: boolean;
  public contactMessage: string;
  public departmentRequired: boolean;
  public departmentMessage: string;
  public text_max: any = 150;
  public text_length: any;
  public text_remaining: any;

  public searchList:any=[
    {
      id:'0',
      value:'contactId',
      label:'EMP_ID'
     },{
      id:'1',
      value:'Name',
      label:'NAME'
     },{
      id:'2',
      value:'mobileNo1',
      label:'MOBILE'
     },{
      id:'3',
      value:'grade',
      label:'GRADE'
     },{
      id:'4',
      value:'project',
      label:'PROJECT'
     },{
      id:'5',
      value:'departmentName',
      label:'DEPARTMENT'
     },{
      id:'6',
      value:'locationName',
      label:'LOCATION'
     }
]

  // public page: number = 0;
  // public phoneDirectories: Array<any>;
  // public pages:Array<number>;
  // public currentIndex: number;
  // public pageStart: number = 1;
  // public pageNumber: number;
  //public mobileNo1:any;
  pageNumber: any = 1;
  totalPage: any;
  size: any = 10;
  totalData: any;
  pageInfo: any = {};
  totalNumberOfData: any;


  name = 'Angular v4 - Applying filters to *ngFor using pipes';
  numberOfData: number;
  limit: number;
  page: number = 1;
  filter: any = {};

  constructor(private itemDataServiceService: ItemDataServiceService, private modelService: NgbModal,
    private getService: GetService, private postService: PostService, private spinner: NgxSpinnerService,
    private router: Router, private app: AppComponent) {
    this.contact.department = 0;
    this.filter.field = 0;
  }



  setPageNext(i, event: any) {

    //this.currentIndex = this.page + 1;
    if (i < this.totalPage) {
      event.preventDefault();
      this.pageInfo.pageNumber = i + 1;
      this.getContactList();
    } else {
      event.preventDefault();
      this.pageInfo.pageNumber = i;
      this.getContactList();
    }

  }
  setPagePrev(i, event: any) {
    //
    //  this.currentIndex = this.page - 1;
    if (i > 1) {
      event.preventDefault();
      this.pageInfo.pageNumber = i - 1;
      this.getContactList();
    } else {
      this.pageInfo.pageNumber = 1;
      this.getContactList();
    }

  }


  ngOnInit() {
  this.filter.field="";
  this.filter.fieldValue="";
    this.loginId = this.itemDataServiceService.getUser();
    this.pageInfo.pageNumber = 1;
    
    this.pageInfo.size = 10;
    this.setPagePrev(1, "")
    //this.getContactList();
    this.getLocationlist();
    this.getAlldepartment();
    // this.getAllMobileList();
  //  this.getContactList1();

  }




  formControl = new FormControl('', [
    Validators.required

  ]);

  newContact(): void {
    // this.submitted = false;
    this.contact = new Contact();
  }
  // getAllMobileList(){
  //   this.getService.getAllMobileList()
  //   .subscribe(data=>{
  //     this.mobliliList=data;
  //     ////console.log(data.status);
  //   },error=>{

  //   })
  // }

  getLocationlist() {
    this.getService.getLocationList().subscribe((locationList) => {
      this.locationList = locationList;
      ////console.log(locationList);

    }, (error) => {
      ////console.log(error);
    }
    );
  }

  getAlldepartment() {
    this.getService.getDepartmentList().subscribe(
      data => {
        this.departmentList = data;
        ////console.log("Fetch All department :  ", data);
        if (data == null) {


        }



      },
      errorCode => {
        ////console.log(errorCode);
      }
    );
  }



  getContactList() {
    //  this.pageInfo.pageNumber=1;
    //  this.pageInfo.size=12;
    let index=this.filter.field;
    if(this.filter.field !=''){
      
      this.filter.field=this.searchList[this.filter.field].value
    }
   
  
    // console.log(this.pageInfo,'gggggggggggggggggggggggggggg',this.filter)
    this.getService.getContactList1(this.pageInfo,this.filter)
      .subscribe(data => {
        this.contactList = data.numberOfData;
        this.totalPage = data.totalNumberOfPage;
        this.numberOfData = this.contactList.length;
        this.totalNumberOfData = data.totalNumberOfData;
        this.limit = this.contactList.length;
       
        this.pageNumber = data.pageNumber;
        this.size = data.pageSize;
        this.filter.field=index;
      }, error => {
   
      });
  }


//   getContactList1() {
//     this.app.checkCredential();
//       this.getService.getContactList().subscribe(data => {
//         console.log("data ",data)
//      this.contactList = data;
// console.log("contact list ",this.contactList)
//       this.noContactCreated = false;

//       if (this.contactList < 1) {
//         this.noContactCreated = true;
//       }
//       else {
//         this.numberOfData= this.contactList.length;
//         this.limit=this.contactList.length;
//         this.noContactCreated = false;
//       }
//     },
//       error => {

//       });
//   }






  onSubmit() {

    this.contact.locationName = this.loginId.locationName;
    //this.contact.departmentName = this.loginId.departmentName;
    if (this.contact.locationName != 0 && !this.contactExists) {
      this.spinner.show();
      this.successField = false;
      this.authenticationError = false;
      this.contact.createDate = new Date();
      this.contact.loginId = this.loginId;
      this.postService.saveContact(this.contact).map((response) => response)
        .subscribe(data => {
          console.log(data);
          this.getContactList();
          if (this.form.valid) {
            ////console.log("Form Submitted!");
            // this.bdayCheck = false;
            // this.anniversarySMSCheck = false;
            this.form.reset();
          }
          if (data.status == 201) {
            // setTimeout(() => {
            this.spinner.hide();
            this.successField = true;
            this.authenticationError = false;
            // }, 2000);
          }
          setTimeout(() => {
            this.successField = false;
          }, 2000);
        },
          error => {
            setTimeout(() => {
              this.spinner.hide();
              this.successField = false;
              this.authenticationError = true;

            }, 2000);
            setTimeout(() => {
              this.authenticationError = false;
            }, 4000);

          });
    }
  }

  updateContact() {
    this.spinner.show();
    this.postService.updateContact(this.contactNumber).map((response) => response)
      .subscribe(data => {
        this.getContactList();
        if (data.status == 201) {

          this.spinner.hide();
          this.successEdit = true;
          this.authenticationError = false;
        }
        setTimeout(() => {
          this.successEdit = false;
          this.closeModel();
        }, 4000);
      },
        error => {
          setTimeout(() => {
            this.spinner.hide();
            this.successEdit = false;
            this.authenticationError = true;
          }, 2000);

          setTimeout(() => {
            this.authenticationError = false;
            this.closeModel();
          }, 4000);

        });


  }

  open(contact, deleteModal) {
    this.dataEmpty();
    this.contact = contact;
    this.myModel = this.modelService.open(deleteModal, { windowClass: 'custom-class ' });
  }

  removeContact(contact) {
    this.contact.departmentName='';
    this.deleteField = false;
    this.getService.removeContact(contact.contactId).map((response) => response)
      .subscribe(data => {
        setTimeout(() => {
         this.closeModel();
        }, 500);
        setTimeout(() => {
          this.closeModel();
          this.deleteField = true;
          this.authenticationError = false;
        }, 500);

        this.getContactList();
        setTimeout(() => {
          this.closeModel();
          this.deleteField = false;
        }, 4000);

      },
        error => {
          setTimeout(() => {
            this.closeModel();
            this.deleteField = false;
            this.authenticationError = true;

          }, 2000);
          setTimeout(() => {
            this.authenticationError = false;
          }, 4000);
        });
  }



  hideAddContact() {
    this.AllContact = false;
    this.dataEmpty();
  }
  showAddContact() {
    this.AllContact = true;
    this.dataEmpty();
  }



  OpenModel(contactNumber, updateModal) {

    this.contactNumber = contactNumber;
    this.myModel = this.modelService.open(updateModal);


  }

  closeModel() {
    this.getContactList();
    this.myModel.close();
  }




  restrictNumeric(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }
  dataEmpty() {
    this.contact.contactId = '';
    this.contact.name = '';
    this.contact.mobileNo1 = '';
    this.contact.empId = '';
    this.contact.dateofbirth = '';
    this.contact.dateofanniversary = '';
    

  }

  changeCheck(value) {

    if (value.target.checked) {
      this.bdayCheck = true;
    } if (!value.target.checked) {
      this.bdayCheck = false;
      // this.contact.schedule = '';
    }

  }

  anniversaryCheck(value) {
    if (value.target.checked) {
      this.anniversarySMSCheck = true;
    } if (!value.target.checked) {
      this.anniversarySMSCheck = false;
    }

  }
  checkValidation() {
    this.contactExists = false;
    if (this.contact.mobileNo1.length == 10) {
      this.getService.getAllMobileList(this.contact.mobileNo1)
        .subscribe((data) => {
          this.mobliliList = data;
          if (this.mobliliList != null) {
            this.contactExists = true;
            this.contactMessage = "already exists!"
          } else {
            this.contactExists = false;
          }
        });
    }


  }
  // focusOutFunction(){
  //   this.empidExists = false;

  //   this.getService.getEmpIdList(this.contact.empId)
  //   .subscribe((data) => {
  //   data
  //   if(data!=null){
  //     this.empidExists = true;
  //     this.empmessage = "already exists!"
  //   }else{
  //     this.empidExists = false;
  //   }

  // });
  //}



  findphoneDirectory() {

      let index=this.filter.field;
      this.filter.field=this.searchList[this.filter.field].value
      this.getService.findPhoneDirectory(this.filter)
      .subscribe(data => {
    //    this.contactList = data;
        console.log('data here contact list find',this.contactList,);
        this.contactList = data.numberOfData;
        this.totalPage = data.totalNumberOfPage;
        this.numberOfData = this.contactList.length;
        this.totalNumberOfData = data.totalNumberOfData;
        this.limit = this.contactList.length;
       
        this.pageNumber = data.pageNumber;
        this.size = data.pageSize;
       debugger;
      }, error => {
     
      });
  }

public list:any;
  refeshPage() {
    this.filter.mobileNo1 = "";
    this.pageInfo.userId = this.loginId.loginId;
    this.setPagePrev(1, "");
    this.filter.fieldValue='';
    this.list.label='';
  }


  filterForeCasts(value,field){
     // this.filter.field=this.searchList[field.field].value;
     
  }
}

