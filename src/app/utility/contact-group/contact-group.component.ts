import { Component, OnInit, group, ViewChild } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { GetService } from '../../services/get.service';
import { PostService } from '../../services/post.service';
import { ContactGroup } from '../../contact-group';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { debug } from 'util';
import { Response } from '@angular/http/src/static_response';
import { AppComponent } from '../../app.component';



@Component({
  selector: 'app-contact-group',
  templateUrl: './contact-group.component.html',
  styleUrls: ['./contact-group.component.css']
})
export class ContactGroupComponent implements OnInit {
  @ViewChild('contactGroupForm') form: any;
  public isAdminTrue: boolean = false;
  public locationList: any = [];
  public find: any = {};
  public noDataFound: boolean = false;
  public choose: any = "undefined";
  public contactgroup: any = {};
  public removebtnhide: boolean = true;
  public tempGroupId: any;
  public disabled: boolean = true;
  public tempgroup1: any = {};
  public idOfDirectory: any = [];
  public nameCheck: boolean = false;
  public contactListforGroup_condition: boolean = false;
  public contactGroupName: any = {};
  public grouplist: any = [];
  public selectedValue = [];
  public contact: any = [];
  public deletesuccess: boolean = false;
  public phoneDirectoryList: any = [];
  public contactList: any = [];
  public new_contactList: any = [];
  public allgroupcontactList: any = [];
  public oneGroupContactList: any = [];
  public checkGroupContactlist: any = [];
  public filterPhoneDirectory: any = [];
  public phoneDirectoryId: any[];
  private user: any = {};
  public loginId: any = {};
  public listOfContact: any;
  public NoContactFilter: boolean;
  public noContactGroupFound:boolean;

  // public ALLGROUPLIST:any=[];
  // public ALLCONTACTLIST:any=[];
  // public findGroupLength:any=[
  //   {
  //     tempgroup:{}
  //   }
  // ];

  public tempgroup: any = {
    groupId: '',
    groupLength: ''
  }
  myModel: any;
  public contactGroup: any = {};
  public successField: any = false;
  public authenticationError: boolean;
  public allChecked: boolean;
  public successEdit: boolean;
  public successCreate: any = false;
  public deleteField: boolean;
  public noGroupCreated: boolean;
  public AllContact: boolean = false;
  public AllGroup: boolean = false;
  public allContactGroupSave: any = {
    contactGroupName: {},
    phoneDirectoryId: [],
    loginId: {}
  };

  public currentGroup: any = '';
  pageNumber: any = 1;
  totalPage: any;
  size: any = 10;
  totalData: any;
  pageInfo: any = {};
  totalNumberOfData: any;
  private name = 'Angular v4 - Applying filters to *ngFor using pipes';
  private numberOfData: number;
  private limit: number;
  private page: number = 1;
  private filter: any = {};


  constructor(private getService: GetService, private postService: PostService, private modelService: NgbModal,
    private itmService: ItemDataServiceService, private spinner: NgxSpinnerService, private app: AppComponent) { }


  setPageNext(i, event: any) {

    //this.currentIndex = this.page + 1;
    if (i < this.totalPage) {
      event.preventDefault();
      this.pageInfo.pageNumber = i + 1;
      this.getGroupListpagination();
    } else {
      event.preventDefault();
      this.pageInfo.pageNumber = i;
      this.getGroupListpagination();
    }

  }
  setPagePrev(i, event: any) {
    //
    //  this.currentIndex = this.page - 1;
    if (i > 1) {
      event.preventDefault();
      this.pageInfo.pageNumber = i - 1;
      this.getGroupListpagination();
    } else {
      this.pageInfo.pageNumber = 1;
      this.getGroupListpagination();
    }

  }



  ngOnInit() {
    this.loginId = this.itmService.getUser();
    //this.showAddOtherLocationContact();
    this.pageInfo.size = 10;
    this.getAllgrouplist();
    this.getLocationlist();
    //this.getGrouplist();
    this.setPagePrev(1, "");
    this.getContactList();

  }

  getGrouplist() {

    this.app.checkCredential();
    this.setPagePrev(1, "");
    // this.getService.getGrouplist().subscribe(data => {
    //   this.grouplist = data.numberOfData;
    //   this.numberOfData = this.grouplist.length;
    //   this.totalPage = data.totalNumberOfPage;
    //   this.limit = this.grouplist.length;
    //   this.pageNumber = data.pageNumber;
    //   this.size = data.pageSize;
    // },
    //   error => {

    //   });

  }



  //group contact list 
  getAllgrouplist() {

    this.getService.getAllgrouplist().subscribe(data => {
      this.allgroupcontactList = data;

    },
      error => {

      });


  }

  getContactList() {

    this.getService.getContactList().subscribe(data => {
      this.contactList = data;
      this.numberOfData = this.contactList.length;
      this.limit = this.contactList.length;
    },
      error => {

      });
  }



  onSubmit() {
    this.spinner.show();
    this.contactgroup.createDate = new Date();
    this.contactgroup.loginId = this.loginId;
    this.postService.createGroup(this.contactgroup).map((response) => response)
      .subscribe(data => {
        this.getGrouplist();
        if (this.form.valid) {
          this.form.reset();
        }
        if (data.status == 201) {

          this.spinner.hide();
          this.successCreate = true;
          this.authenticationError = false;
        }
        setTimeout(() => {
          this.successCreate = false;
        }, 2000);
      },
        error => {


          setTimeout(() => {
            this.spinner.hide();
            this.successCreate = false;
            this.authenticationError = true;

          }, 2000);


          setTimeout(() => {
            this.authenticationError = false;
          }, 4000);

        });


  }


  //add contact number in group
  addContact_In_Group() {

    this.disabled = true;
    this.removebtnhide = false;
    this.spinner.show();
    this.tempGroupId = this.contactgroup.contact_groupId;
    this.contactgroup.loginId = this.loginId;
    this.allContactGroupSave.contactGroupName = this.contactgroup;
    this.allContactGroupSave.phoneDirectoryId = this.new_contactList;
    this.allContactGroupSave.loginId = this.loginId;
    this.postService.addContactinGroup(this.allContactGroupSave).map((response) => response)
      .subscribe(data => {

        if (data.status == 200) {
          // setTimeout(() => {
          this.spinner.hide();
          this.getOneGroupcontactListById(this.tempGroupId)
          this.authenticationError = false;
          this.AllGroup = true;
          this.contactListforGroup_condition = false;
          // }, 2000);
        }
        this.dataEmpty();
      },
        error => {
          setTimeout(() => {
            this.spinner.hide();
            this.successField = false;
            this.authenticationError = true;
          }, 1000);

          setTimeout(() => {
            this.authenticationError = false;
          }, 4000);

        });
    ////console.log("add id  ", this.tempGroupId);
    // this.contactListforGroup_condition = false;
    this.dataEmpty();
    this.new_contactList = [];
    this.allChecked = false;
  }

  //  ------------------------------------------Group Remove---------------------------

  removeGroup(group) {


    this.deleteField = false;
    this.getService.removeGroup(group.contact_groupId).map((response) => response)
      .subscribe(data => {
        //console.log(data);
        setTimeout(() => {
          this.closeModel();
          this.deleteField = true;
          this.authenticationError = false;
          this.getGroupListpagination();
          
        }, 500);



        setTimeout(() => {
          // this.spinner.hide();
          this.deleteField = false;
        }, 3000);
        this.getGrouplist()
      },
        error => {
          setTimeout(() => {
            //this.spinner.hide();
            this.deleteField = false;
            this.authenticationError = true;
          }, 3000);
          setTimeout(() => {
            this.authenticationError = false;
          }, 5000);
        });
  }


  showContactList(group) {
    this.find.locationName = this.choose;
    this.filter.contactGroupName = '';
    this.spinner.show();
    this.contactgroup = group;
    this.getService.getContactListNotInGroup(group.contact_groupId)
      //.map((response)=>response)
      .subscribe(data => {
        setTimeout(() => {
          this.spinner.hide();

          this.filterPhoneDirectory = data;
          this.numberOfData = this.filterPhoneDirectory.length;
          this.limit = this.filterPhoneDirectory.length;
          ////console.log("phone filter directory list " + this.filterPhoneDirectory);
          this.contactListforGroup_condition = true;
          if (this.filterPhoneDirectory < 1) {
            this.NoContactFilter = true;
            this.noDataFound = false;
            this.noContactGroupFound=false;
          
          }
          else {
            this.NoContactFilter = false;
            this.noDataFound = false;
            this.noContactGroupFound=false;
          }
        }, 2000)
      }, error => {
        setTimeout(() => {
          this.spinner.hide();
        }, 2000)
        ////console.log("one group data errror", error);
      });
  }


  // showContactList1(group) {
  //   this.spinner.show();
  //   this.contactgroup=group;

  //   this.getOneGroupContactList(group)

  //   setTimeout(() => {
  //     this.filterPhoneDirectory=[];
  //     this.AllGroup = false;
  //     this.contactListforGroup_condition = true;
  //     ////console.log("contact check purposin group ", this.checkGroupContactlist);
  // ////console.log(" this.contactList", this.contactList)
  // let check=0;
  // for(let i in this.contactList){
  //   for(let j in this.checkGroupContactlist){
  //     if(this.contactList[i].mobileNo1 == this.checkGroupContactlist[j].phoneDirectoryId.mobileNo1)
  //     {
  //       ////console.log(this.contactList[i],"  true    ",this.checkGroupContactlist[j]);
  //       check=1;
  //       break;
  //     }else{
  //      check=0;
  //       ////console.log(this.contactList[i]," false     ",this.checkGroupContactlist[j]);
  //     }
  //   }
  //   if(check==0){
  //     this.filterPhoneDirectory.push(this.contactList[i]);
  //   }

  // }
  // this.numberOfData=this.filterPhoneDirectory.length;
  // this.limit=this.filterPhoneDirectory.length;
  // this.spinner.hide();
  //   },5000);

  //     this.AllGroup = false;
  //     this.contactListforGroup_condition = true;
  //     ////console.log("contact check purposin group ", this.checkGroupContactlist);
  // ////console.log(" this.contactList", this.contactList)

  // }

  // -------------------------------get All group list by Contact-------------------------

  getOneGroupContactList(group) {
    this.removebtnhide = true;
    this.spinner.show();
    this.tempgroup1 = group;
    this.currentGroup = this.tempgroup1.contactGroupName;
    this.deletesuccess = false;
    this.numberOfData = 0;
    this.limit = 0;
    this.phoneDirectoryList = [];
    this.idOfDirectory = [];
    // this.findGroupLengthByContact();

    this.getService.getOneGroupContactList(group.contact_groupId)
      //.map((response)=>response)
      .subscribe(data => {
        //console.log("one groupone data ", data);
        setTimeout(() => {
          this.spinner.hide();
          this.AllGroup = true;
          this.oneGroupContactList = data;
          this.checkGroupContactlist = data;
          ////console.log("length ", this.oneGroupContactList);
          this.numberOfData = this.oneGroupContactList.length;
          this.limit = this.oneGroupContactList.length;
          ////console.log("phone directory list " + this.oneGroupContactList);

        }, 2000)
      }, error => {
        setTimeout(() => {
          this.spinner.hide();
        }, 2000)
        ////console.log("one group data errror", error);
      });
    // ////console.log("contact in group ",this.oneGroupContactList);

  }
  // --------------------------------For Phonedirectory Filter By Id-----------------------

  getOneGroupcontactListById(id) {
    this.phoneDirectoryList = [];
    this.getService.getOneGroupContactList(id)
      .subscribe(data => {
        ////console.log("one id group data ", data);
        this.oneGroupContactList = data;
        this.numberOfData = this.oneGroupContactList.length;
        this.limit = this.oneGroupContactList.length;
        setTimeout(() => {
          this.successField = true;
          this.successCreate = false;
          this.tempgroup1 = this.oneGroupContactList[0].contactGroupName;
          //  this.currentGroup= this.tempgroup1.contactGroupName;
          for (let i in this.oneGroupContactList) {
            this.idOfDirectory.push(this.oneGroupContactList[i].id);
           
            this.phoneDirectoryList.push(this.oneGroupContactList[i].phoneDirectoryId);
          }
        }, 500);
        setTimeout(() => {
          this.successField = false;

        }, 2000)
      }, error => {
        setTimeout(() => {
          this.spinner.hide();

        }, 2000)
        ////console.log("one group data errror", error);
      });
    ////console.log("contact in group ", this.oneGroupContactList);
    this.AllGroup = true;
  }


  // ------------------------after addding contact remove contact from group-----------------------------


  removeContactFromGroup(id) {
    //group.ontactGroupName.contact_group
    this.tempGroupId = this.tempgroup1.contact_groupId;
    this.currentGroup = this.tempgroup1.contactGroupName;
    this.getService.removeContactFromGroup(id, this.tempGroupId).map((response) => response)
      .subscribe(data => {
        ////console.log("haraaaaa  ", data);
        // if (data.status == 200) {
        this.phoneDirectoryList = [];
        this.getOneGroupContactList(this.tempgroup1);
        this.deletesuccess = true;
        ////console.log("data success fully delete");
        // }
        //  
      }, error => {
        ////console.log(error);
      })
    ////console.log("remoce id ", this.tempGroupId);

    //this.getOneGroupcontactListById(this.tempGroupId);

  }


  BackContactGroupList() {
   // this.getGrouplist();
  this.setPagePrev(1,"");
    
    this.find.locationName = this.choose;
    this.filter.name = '';
    this.filter.mobileNo1 = '';
    this.find.mobile = '';
    this.find.name = '';
    this.allChecked = false;
    this.AllGroup = false;
    this.contactListforGroup_condition = false;
    this.dataEmpty();

  }

  showCreateGroup() {
    this.contactListforGroup_condition = true;
    this.AllGroup = true;
  }

  updateContactGroup() {

    this.spinner.show();
    this.postService.updateGroup(this.contactGroup).map((response) => response)
      .subscribe(data => {
        this.getGrouplist();
        ////console.log(data);
        if (data.status == 201) {
          // setTimeout(() => {
          this.spinner.hide();
          this.successEdit = true;
          this.authenticationError = false;
          //}, 2000);
          this.dataEmpty();
        }
        setTimeout(() => {
          this.successEdit = false;
          this.closeModel();
        }, 2000);
      },
        error => {


          setTimeout(() => {
            this.spinner.hide();
            this.successEdit = false;
            this.authenticationError = true;
          }, 2000);

          setTimeout(() => {
            this.authenticationError = false;
            this.closeModel();
          }, 4000);

        });

  }


  selectAllChecked(i) {
    if (i.target.checked) {

    } else {
      this.new_contactList = [];
    }


    this.selectcontactGroup(this.allChecked);

    // if (i.target.checked) {

    //   //console.info(i.target.checked);
    //   this.new_contactList.push(filterPhoneDirectory);
    // } else {
    //   this.new_contactList.splice(filterPhoneDirectory, 1);
    // }

    // if (this.new_contactList.length > 0) {
    //   this.disabled = false;

    // }
    // else {
    //   this.disabled = true;
    // }

    // //console.info(this.new_contactList);
  }
  // }

  selectcontactGroup(value: boolean) {
    if (value) {
      for (var i = 0; i < this.filterPhoneDirectory.length; i++) {
        this.filterPhoneDirectory[i].selected = this.selectAllChecked
        this.disabled = false;
      }
      this.new_contactList = this.filterPhoneDirectory;
      console.log("new contact", this.new_contactList);
      console.log("filter contact", this.filterPhoneDirectory);
    } else {
      for (var i = 0; i < this.filterPhoneDirectory.length; i++) {
        this.filterPhoneDirectory[i].selected = '';
        this.disabled = true;
      }
      this.new_contactList = [];
      console.log("new contact", this.new_contactList);

    }
  }

  changeCheck(i, contact) {
    if (i.target.checked) {
      this.new_contactList.push(contact);
    } else {
      this.new_contactList.splice(contact, 1);
    }
    if (this.new_contactList.length > 0) {
      this.disabled = false;
    }
    else {
      this.disabled = true;
    }
  }

  dataEmpty() {
    this.allContactGroupSave = {};
    this.contactgroup.contact_groupId = '';
    this.contactgroup.contactGroupName = '';
    this.contactgroup.abbreviation = '';
  }

  OpenModel(contactGroup, updateModal) {
    this.contactGroup = contactGroup;
    this.myModel = this.modelService.open(updateModal);


  }

  closeModel() {
    this.getGrouplist();
    this.myModel.close();
  }


  // findGroupLengthByContact(){
  // //  



  //   for(let i in this.grouplist){
  //    let lengthCount=0;

  //     for(let j in this.allgroupcontactList){
  //       if(this.grouplist[i].contact_groupId ==this.allgroupcontactList[j].contactGroupName.contact_groupId){
  //         lengthCount++;
  //       }
  //     }
  //     ////console.log(this.grouplist[i].contact_groupId ,"      ",lengthCount)
  //     this.tempgroup.groupId="G"+this.grouplist[i].contact_groupId;
  //     this.tempgroup.groupLength=lengthCount;
  //     ////console.log("kkkkkkkk      ",this.tempgroup)

  //   // this.ALLGROUPLIST.push(this.tempgroup);
  //     }

  //   //////console.log("all group lenght  => ",this.findGroupLength);
  // }


  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);
  public group: any;
  open(group, deleteModal) {

    this.group = group;
    ////console.log("***********************" ,contactgroup);
    this.myModel = this.modelService.open(deleteModal, { windowClass: 'custom-class ' });
  }


  //**********************************other location contact list ************************************** */

  public findLocationWiseContact() {
    this.allChecked = false;
    this.find.groupId = this.contactgroup.contact_groupId;
    this.getService.getFindPhoneDirectoryOfOtherLocation(this.find).subscribe(data => {
      this.filterPhoneDirectory = data;
      this.numberOfData = this.filterPhoneDirectory.length;
      this.limit = this.filterPhoneDirectory.length;
      if (this.limit < 1) {
        this.noDataFound = true;
        this.NoContactFilter = false;
      }
      else {
        this.noDataFound = false;
        this.NoContactFilter = false;
      }
      //console.log("limit ",this.limit)

      ////console.log("phone filter directory list " + this.filterPhoneDirectory);
      this.contactListforGroup_condition = true;

    }, error => {
      //console.info("error r",error);
    });
  }
  getLocationlist() {
    this.getService.getLocationList().subscribe((locationList) => {
      this.locationList = locationList;
      //console.log(locationList);
      //   if(locationList.length == 0)
      //   {



      // }
    }, (error) => {
      ////console.log(error);
    }
    );
  }


  // showAddOtherLocationContact(){
  //   if(this.loginId.userType == "user")
  //   {
  //     this.isAdminTrue=false;
  //   }else{
  //     this.isAdminTrue=true;
  //   }
  // }

  getGroupListpagination() {

    this.pageInfo.userId = this.loginId.loginId
    this.getService.getGroupListpagination(this.pageInfo).subscribe(data => {

      
      this.grouplist = data.groupsList;
      this.totalPage = data.totalNumberOfPage;
      this.limit = this.grouplist.length;
      this.pageNumber = data.pageNumber;
      this.size = data.pageSize;
      this.numberOfData = this.grouplist.length;

    }, error => {
    
    });
  }


  refeshPage() {
    this.filter.contactGroupName = "";
    this.pageInfo.userId = this.loginId.loginId;
    this.setPagePrev(1, "");
  }

  findGroupOnkeyPress(){

    console.info("  ",this.pageInfo.groupName);
  //   this.pageInfo.userId = this.loginId.loginId;
     this.setPagePrev(1, "");
  }

}
