import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DndMasterComponent } from './dnd-master.component';

describe('DndMasterComponent', () => {
  let component: DndMasterComponent;
  let fixture: ComponentFixture<DndMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DndMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DndMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
