import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineKeywordComponent } from './define-keyword.component';

describe('DefineKeywordComponent', () => {
  let component: DefineKeywordComponent;
  let fixture: ComponentFixture<DefineKeywordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefineKeywordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineKeywordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
