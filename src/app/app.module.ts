import { CdkTableModule } from '@angular/cdk/table';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule,FormGroup,FormControl,ReactiveFormsModule} from '@angular/forms'
import { TooltipModule } from 'ngx-bootstrap';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {      
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,     
} from '@angular/material';  
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';  
import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router'
import { AppComponent } from './app.component';
import { Route } from '@angular/compiler/src/core';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {AdminLoginComponent} from './admin/admin-login/admin-login.component';
import {HomeComponent} from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import {AccountStatusComponent} from './admin/account-status/account-status.component';
import{DepartmentMasterComponent} from './admin/department-master/department-master.component';
import{LocationMasterComponent} from './admin/location-master/location-master.component';
import{MobileMasterComponent} from './admin/mobile-master/mobile-master.component';
import{ChangePasswordComponent} from './admin/change-password/change-password.component';
import {SmtpMailConfComponent} from './admin/smtp-mail-conf/smtp-mail-conf.component';
import{UserMasterComponent} from './admin/user-master/user-master.component';
import {BackupComponent} from './utility/backup/backup.component';
import {ContactGroupComponent} from './utility/contact-group/contact-group.component';
import {DndMasterComponent} from './utility/dnd-master/dnd-master.component';
import {PhoneDirectoryComponent} from './utility/phone-directory/phone-directory.component';
import { SmsVoiceDraftComponent } from './sms-voice/sms-voice-draft/sms-voice-draft.component';
import { UploadVoiceFileComponent } from './sms-voice/upload-voice-file/upload-voice-file.component';
import { ClientIdMasterComponent } from './sms-voice/client-id-master/client-id-master.component';
import { InstantMessageComponent } from './sms-voice/instant-message/instant-message.component';
import { InstantVoiceComponent } from './sms-voice/instant-voice/instant-voice.component';
import { ScheduleMessageComponent } from './sms-voice/schedule-message/schedule-message.component';
import { ScheduleVoiceComponent } from './sms-voice/schedule-voice/schedule-voice.component';
import { InstantMessageReportComponent } from './sms-voice/instant-message-report/instant-message-report.component';
import { InstantVoiceReportComponent } from './sms-voice/instant-voice-report/instant-voice-report.component';
import { ScheduleMessageReportComponent } from './sms-voice/schedule-message-report/schedule-message-report.component';
import { ScheduleVoiceReportComponent } from './sms-voice/schedule-voice-report/schedule-voice-report.component';
import { MessageTrunkReportComponent } from './sms-voice/message-trunk-report/message-trunk-report.component';
import { ConfigureKeywordComponent } from './keyword-manager/configure-keyword/configure-keyword.component';
import { PullReportComponent } from './keyword-Manager/pull-report/pull-report.component';
import { DefineQueryComponent } from './auto-pull-manager/define-query/define-query.component';
import { DefineKeywordComponent } from './Auto-Pull-Manager/define-keyword/define-keyword.component';
import { HelpComponent } from './help/help.component';
import { Component } from '@angular/core/src/metadata/directives';
import { ReportComponent } from './all-report/report/report.component';
import { GetService } from './services/get.service';
import { PostService } from './services/post.service';
import { ItemDataServiceService } from './services/item-data-service.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import 'rxjs/add/operator/map'
import {Subject} from 'rxjs/Subject';
import { HttpModule} from '@angular/http';
import { HttpClientModule} from '@angular/common/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import {DataFilterPipe} from './shared/data-filter-pipe';
import { ScheduleSmsReportComponent } from './all-report/schedule-sms-report/schedule-sms-report.component';
import { LatestnewsComponent } from './admin/latestnews/latestnews.component';
import { ErrorpageComponent } from './admin/errorpage/errorpage.component';
import { AllcontactComponent } from './utility/allcontact/allcontact.component';
import { MessageDetailComponent } from './message-detail/message-detail.component';
import { NgxPaginationModule } from 'ngx-pagination';
//import { LoadingModule} from 'ngx-loading';
import { PaginationModule } from 'ngx-bootstrap/pagination';




const app_Routers :any=[
 
  {
    path:'',
    component:AdminLoginComponent

},
{
  path:'error',
  component:ErrorpageComponent
},

{
  path:'home',
  component:HomeComponent

},
{
  path:'admin/account/status',
  component:AccountStatusComponent
},
{
  path:'admin/department/master',
  component:DepartmentMasterComponent
},{
  path:'admin/change/password',
  component:ChangePasswordComponent
},{
  path:'admin/location/master',
  component:LocationMasterComponent
},{
  path:'admin/mobile/master',
  component:MobileMasterComponent
},{
  path:'admin/smtpmail/conf',
  component:SmtpMailConfComponent
},{
  path:'admin/user/master',
  component:UserMasterComponent
},{
  path:'utility/backup',
  component:BackupComponent
},{
  path:'utility/contact/group',
  component:ContactGroupComponent
},{
  path:'utility/dnd/master',
  component:DndMasterComponent
},{
  path:'utility/phone/directory',
  component:PhoneDirectoryComponent
},{
  path:'sms/voice/draft',
  component:SmsVoiceDraftComponent
},{
    path:'sms/voice/file',
    component:UploadVoiceFileComponent
},{
     path:'sms/client/master',
     component:ClientIdMasterComponent
 },
{
    path:'sms/message/instant/message',
    component:InstantMessageComponent
},{
  path:'sms/voice/instant/message',
  component:InstantVoiceComponent
},{
  path:'sms/schedule/message',
  component:ScheduleMessageComponent
},{
  path:'sms/schedule/voice',
  component: ScheduleVoiceComponent
},{
  path:'sms/instant/message/report',
  component: InstantMessageReportComponent
},{
  path:'sms/instant/voice/report',
  component: InstantVoiceReportComponent
},{
  path:'sms/schedule/message/report',
  component: ScheduleMessageReportComponent
},{
  path:'sms/schedule/voice/report',
  component: ScheduleVoiceReportComponent
},{
  path:'sms/message/trunk/report',
  component: MessageTrunkReportComponent
},{
   path:'keyword/configure/keyword',
   component:ConfigureKeywordComponent
},{
  path:'keyword/pull/report',
  component:PullReportComponent
},{
  path:'pull/manager/define/query',
  component:DefineQueryComponent
},{
  path:'pull/manager/define/keyword',
  component:DefineKeywordComponent
},{
  path:'help',
  component:HelpComponent
},{
  path:'report',
  component:ReportComponent
},
{
  path:'schedule/sms/report',
  component:ScheduleSmsReportComponent
},{
path:'admin/latest/news',
component:LatestnewsComponent
},{
  path:'utility/allcontact',
  component:AllcontactComponent
  },{
    path:'message/detail',
    component:MessageDetailComponent
    }
];

@NgModule({
  declarations: [
    
    AppComponent,
    AdminLoginComponent,
    HomeComponent,
    MenuComponent,
    AccountStatusComponent,
    ChangePasswordComponent,
    DepartmentMasterComponent,
    LocationMasterComponent,
    MobileMasterComponent,
    SmtpMailConfComponent,
    UserMasterComponent,
    BackupComponent,
    ContactGroupComponent,
    DndMasterComponent,
    PhoneDirectoryComponent,
    SmsVoiceDraftComponent,
    UploadVoiceFileComponent,
    ClientIdMasterComponent,
    InstantMessageComponent,
    InstantVoiceComponent,
    ScheduleMessageComponent,
    ScheduleVoiceComponent,
    InstantMessageReportComponent,
    InstantVoiceReportComponent,
    ScheduleMessageReportComponent,
    ScheduleVoiceReportComponent,
    MessageTrunkReportComponent,
    ConfigureKeywordComponent,
    PullReportComponent,
    DefineQueryComponent,
    DefineKeywordComponent,
    HelpComponent,
    ReportComponent,
    DataFilterPipe,
    ScheduleSmsReportComponent,
    LatestnewsComponent,
    ErrorpageComponent,
    AllcontactComponent,
    MessageDetailComponent,

    
    
    
  ],
  imports: [
    FormsModule,
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    BrowserModule,
    FormsModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    RouterModule.forRoot(app_Routers),
 // RouterModule.forRoot(app_Routers,{ enableTracing: true }),
    AngularFontAwesomeModule,
    HttpModule,
    HttpClientModule,
    NgbModule.forRoot(),
    NgxSpinnerModule,
    CdkTableModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    NgxPaginationModule
   
    // LoadingModule,
    
    

  ],
  exports: [   
       
    CdkTableModule,
    BrowserAnimationsModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,    
  ],    
  providers: [ItemDataServiceService,GetService,PostService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
