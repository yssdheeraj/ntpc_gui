import { Component, ViewChild, OnInit } from '@angular/core';
import { GetService } from '../../services/get.service';
import { PostService } from '../../services/post.service';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { Department } from '../../shared/department';
import { DatePipe } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { error } from 'util';
import { NgxSpinnerService } from 'ngx-spinner';
import { empty } from 'rxjs/Observer';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { Observable } from 'rxjs/Observable';
import { SelectionModel } from '@angular/cdk/collections';
import {FormsModule,FormGroup,FormControl,Validators} from '@angular/forms';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-department-master',
  templateUrl: './department-master.component.html',
  styleUrls: ['./department-master.component.css']
})
export class DepartmentMasterComponent{



  
  public allDepartment: any = [];
  public loginId: any = {};
  public departmentId: any;
  public myModel: any;
  public department: any = {};
  public empty: any = {};
  public statusCode: number;
  public result1: boolean = false;
  public result2: any;
  public successField: any = false;
  public authenticationError: boolean;
  public successEdit: boolean;
  public deleteField: boolean;
  public nodepartmentCreated: boolean;
  public updateON: boolean = false;

  //displayedColumns = ['select','departmentId', 'departmentName', 'departmentDesc','createdDate','actions'];
  
  // data = Object.assign(this.department);  
  // dataSource = new MatTableDataSource<any>(this.data);
  // selection = new SelectionModel<any>(true, []);

  
  //DataSource;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

numberOfData: number;
  limit: number;
  page: number = 1;
  filter:any={};
 
  constructor(private getService: GetService, private modelService: NgbModal, private postService: PostService,
    private itemDataServiceService: ItemDataServiceService, private spinner: NgxSpinnerService,private app:AppComponent) {

      ////console.log("Dheeraj Sharma" ,this.data);
  }

   
   /** Whether the number of selected elements matches the total number of rows. */
  //  isAllSelected() {
  //    
  //   const numSelected = this.selection.selected.length;
  //   const numRows = this.dataSource.data.length;
  //   return numSelected === numRows;
  // }
  // removeSelectedRows() {
  // 
  //   this.selection.selected.forEach(item => {
  //     let index: number = this.data.findIndex(d => d === item);
  //     //console.log(this.data.findIndex(d => d === item));
  //     this.data.slice(index)
  //     this.dataSource = new MatTableDataSource<Element>(this.data);
  //   });
  //   this.selection = new SelectionModel<Element>(true, []);
  // }
  /** Selects all rows if they are not all selected; otherwise clear selection. */
  // masterToggle() {
  //   
  //   this.isAllSelected() ?
  //     this.selection.clear() :
  //     this.dataSource.data.forEach(row => this.selection.select(row));
  // }


  ngOnInit() {
    this.loginId = this.itemDataServiceService.getUser();
    this.getAlldepartment();

    // this.dataSource.paginator = this.paginator;
    // this.dataSource.sort = this.sort;

  }

  // refresh() {
  //   this.getAlldepartment();
  // }


  // applyFilter(filterValue: string) {
  //   filterValue = filterValue.trim();
  //   filterValue = filterValue.toLowerCase();
  //   this.dataSource.filter = filterValue;
  // }

  OpenModel(department, updateModal) {
    this.departmentExists=false;
    this.updateON = true;
    this.department = department;
    ////console.log(department);
    this.myModel = this.modelService.open(updateModal ,  { windowClass: 'custom-class ' });

  }
  closeModel() {
    this.departmentExists=false;
    this.getAlldepartment();
    this.myModel.close();
    this.department = this.empty;
    ////console.log(this.department);

  }

  addDepartment() {
 
    // this.itemDataServiceService.getUser();
    //  this.loginId=this.itemDataServiceService.getUser();
    this.department.loginId = this.loginId;
    if(this.department.departmentName!=undefined)
    {
      this.spinner.show();
    //this.department.createdDate = new Date();
    ////console.log("Department : ", this.department);
    this.postService.saveDepartment(this.department).map((response) => response).subscribe(data => {
      this.getAlldepartment();
      ////console.log("Add Departement :", data);
      if (data.status == 201) {
          this.spinner.hide();
          this.successField = true;
          this.authenticationError = false;
        this.department.departmentName = '';
        this.department.departmentDesc = '';

      }
      setTimeout(() => {
        this.successField = false;
       // this.closeModel();
      }, 2000);

    },
      error => {

        setTimeout(() => {
          this.spinner.hide();
          this.successField = false;
          this.authenticationError = true;

        }, 2000);


        setTimeout(() => {
          this.authenticationError = false;
          //this.closeModel();
        }, 4000);

      });


  }

  }

  updateDepartment() {
    this.departmentExists=false;
    this.spinner.show();
    this.department.createdDate = new Date();
    this.postService.updateDepartment(this.department).map(
      (response) => response
    ).subscribe(
      data => {
        this.getAlldepartment();
        ////console.log(data);
        if (data.status == 201) {
          // setTimeout(() => {
            this.spinner.hide();
            this.successEdit = true;
            this.authenticationError = false;
          // }, 2000);

          this.department.departmentName = '';
          this.department.departmentDesc = '';

        }
        setTimeout(() => {
          this.successEdit = false;
          this.closeModel();
        }, 4000);

      },
      error => {

        setTimeout(() => {
          this.spinner.hide();
          this.successEdit = false;
          this.authenticationError = true;

        }, 2000);


        setTimeout(() => {
          this.authenticationError = false;
          this.closeModel();
        }, 4000);

      });


  }



getAlldepartment(){
  this.app.checkCredential();
  this.getService.getDepartmentList()
  .subscribe(data => {
     this.allDepartment= data;
     // this.dataSource.data = data;
      this.nodepartmentCreated = false;
      ////console.log("Fetch All department :  ", this.allDepartment);
      ////console.log('this.department.length=' + this.allDepartment.length);
      // this.numberOfData = this.allDepartment.length;
      // this.limit = this.allDepartment.length;
     // //console.log('this.department.length = ' + this.dataSource.data.length);
      if (this.allDepartment < 1) {
        this.nodepartmentCreated = true;
      }
      else {
        this.numberOfData= this.allDepartment.length;
        this.limit=this.allDepartment.length;
        this.nodepartmentCreated = false;
      }
    }
    ,

    errorCode => {
      ////console.log("Error Code #### " ,errorCode);
    });
    //console.log(this.allDepartment);
}






  deleteDepartment(department) {
   // this.spinner.show();
    this.deleteField = false;

    this.getService.deleteDepartment(department.departmentId).
      subscribe(data => {
        setTimeout(() => {
          this.closeModel();
        }, 500);
        setTimeout(() => {
         // this.spinner.hide();
          this.deleteField = true;
          this.authenticationError = false;
        }, 500);

        this.getAlldepartment();
        setTimeout(() => {
          this.deleteField = false;
        }, 4000);

      },
        error => {
          setTimeout(() => {
            //this.spinner.hide();
            this.deleteField = false;
            this.authenticationError = true;

          }, 2000);


          setTimeout(() => {
            this.authenticationError = false;
          }, 4000);
        });
  }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

public departmentExists:boolean;
public departmentMessage:string;

// checkDepartment()
// {
//   this.departmentExists = false;
//   for (let i in this.allDepartment) {

//     if (this.allDepartment[i].departmentName == this.department.departmentName) {
//       this.departmentExists = true;
//       this.departmentMessage = "Already exists!";
//       break;
//     }
//     else {
//       this.departmentExists = false;
//     }
//   }if(this.departmentExists && !this.updateON){
//       this.departmentMessage = "Already exists!";
//     }else{
//       this.departmentExists = false;
//     }
  
// }

 
open(department,deleteModal) {
   
  this.department = department;
 ////console.log("***********************" ,sms);
   this.myModel = this.modelService.open(deleteModal ,  { windowClass: 'custom-class ' });
}
 

}


