import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { GetService } from '../../services//get.service';
import { PostService } from '../../services/post.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule, FormGroup, Validators } from '@angular/forms'
import { error, Key } from 'selenium-webdriver';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-user-master',
  templateUrl: './user-master.component.html',
  styleUrls: ['./user-master.component.css'],

})
export class UserMasterComponent implements OnInit {


  public mobileList:any[];  
  public accessLocation:boolean=true;
  public accessMobileMaster:boolean=true;
  public accessDeparment:boolean=true;
  public accessUser:boolean=true;
  public myLoginId:any;
  public location1:any={};
  public department1:any={};
  public isContactExsit=false;
  public isLocationValid=false;
  public isDepartmentValid=false;
  public isUserTypeValid=false;
  public isfullfill=false;
  public departmentMessage:any;
  public userMessage:String= "Please select user";
  public isSuperAdmin:boolean=false;
  public isAdmin:boolean=false;
  public isUser:boolean=false;
  public departmentList:any=[];
  public department:any={};
  public depaartmentName:any;

  public disabled: boolean = false;
  public userForm: FormGroup;
  public successmsg: boolean = false;
  public errormsg: boolean = false;
  public errorpassword: boolean = false;
  public updatesuccessmsg: boolean = false;
  public deletesuccessmsg: boolean = false;
  public updateON: boolean = false;
  public USERSREPORT: boolean = false;
  public userDetailsShow: boolean = false;
  public all: boolean = true;
  public authenticationError: boolean;
  public locationRequired: boolean;
  public locationMessage: string;
  public userRequired: boolean;
  public departmentRequired: boolean=false;
  public contactMessage:string;
  public contactExists:boolean;

  private userMasterAllView: boolean;

  private adduserMasterView: boolean;
  private viewuserMasterView: boolean;
  private modifyuserMasterView: boolean;
  private deleteuserMasterView: boolean;

  private allowMobileMasterAllView: boolean;
  private addmobileMasterView: boolean;
  private viewmobileMasterView: boolean;
  private modifymobileMasterView: boolean;
  private deletemobileMasterView: boolean;

  public locationAllView:boolean;
  public addlocationView:boolean;
  public viewlocationView:boolean;
  public modifylocationView:boolean;
  public deletelocationView:boolean;

  public departmentAllView:boolean;
  public adddepartmentView:boolean;
  public viewdepartmentView:boolean;
  public modifydepartmentView:boolean;
  public deletedepartmentView:boolean;
  private phoneDirectoryAllView: boolean;
  private addphoneDirectoryView: boolean;
  private viewphoneDirectoryView: boolean;
  private modifyphoneDirectoryView: boolean;
  private deletephoneDirectoryView: boolean;

  private contactGroupAllView: boolean;
  private addcontactGroup: boolean;
  private viewcontactGroup: boolean;
  private modifycontactGroupView: boolean;
  private deletecontactGroupView: boolean;

  private smsDraftAllView: boolean;
  private addsmsDraftView: boolean;
  private viewsmsDraftView: boolean;
  private modifysmsDraftView: boolean;
  private deletesmsDraftView: boolean;


  private instantMessageAllView: boolean;
  private addinstantMessageView: boolean;

  private scheduleMessageAllView: boolean;
  private addscheduleMessageView: boolean;
  private viewscheduleMessageView: boolean;
  private modifyscheduleMessageView: boolean;
  private deletescheduleMessageView: boolean;

  private scheduleVoiceAllView: boolean;

  private instantMessageReportAllView: boolean;
  private viewinstantMessageReportView: boolean;
  private modifyinstantMessageReportView: boolean;
  private deleteinstantMessageReportView: boolean;

  private instantvoiceReportAllView: boolean;
  private viewinstantvoiceReportView: boolean;

  private scheduleMessageReportAllView: boolean;
  private viewscheduleMessageReportView: boolean;

  private scheduleVoiceReportAllView: boolean;
  private viewscheduleVoiceReportView: boolean;

  private allPrivilegeView: boolean;

  public userList: any[];
  public locationName: any;
  public user: any = {};
  public emptyUser: any = {}
  private location: any = {};
  public locationList: any[];
  public loginId: any = {};
  private createBY: any;

  private name = 'Angular v4 - Applying filters to *ngFor using pipes';
  private numberOfData: number;
  private limit: number;
  private page: number = 1;
  private filter: any = {};

  myModel: any;
  constructor(private router: Router, private itemDataServiceService: ItemDataServiceService, private getService: GetService, private postService: PostService,
    private modelService: NgbModal, private spinner: NgxSpinnerService
    ,private app:AppComponent) {
    this.location1.locationName1 = 0;
    this.department1.departmentName1 = 0;
    this.user.userType = 0;

  }

  ngOnInit() {
    this.loginId = this.itemDataServiceService.getUser();
    this.myLoginId=this.loginId.loginId;
    this.checkUserType(this.loginId.userType);
    this.getLocationlist();
    this. getDepartmentList();
    this.getuserList();
    ////console.log("locatio list " + this.locationList);

  }



  isallPrivilegeChecked() {
    ////console.log("####isallPrivilegeChecked######", this.user.allPrivilegeView);
    this.userMasterselectAll(this.user.allPrivilegeView)
    this.allowMobileMasterselectAll(this.user.allPrivilegeView);
    this.phoneDirectoryselectAll(this.user.allPrivilegeView);
    this.contactGroupselectAll(this.user.allPrivilegeView);
    this.smsDraftselectAll(this.user.allPrivilegeView);
    this.instantMessageselectAll(this.user.allPrivilegeView);
    this.scheduleMessageselectAll(this.user.allPrivilegeView);
    this.instantMessageReportselectAll(this.user.allPrivilegeView);
    this.scheduleMessageReportselectAll(this.user.allPrivilegeView);
    this.instantvoiceReportselectAll(this.user.allPrivilegeView);
    this.scheduleVoiceReportselectAll(this.user.allPrivilegeView);
    this.locationselectAll(this.user.allPrivilegeView);
    this.departmentselectAll(this.user.allPrivilegeView);
  }

  // allFieldSelect(value:boolean){
  //   ;
  //   if(value){
  //     this.userMasterAllView=true;
  //   }
  //   if(!value){
  //     this.userMasterAllView=false;
  //   }

  //}




  isuserMasterChecked() {
    ////console.log("####isuserMasterChecked######", this.user.userMasterAllView);
    this.userMasterselectAll(this.user.userMasterAllView);
  }

  userMasterselectAll(value: boolean) {
    if (value) {
      this.user.adduserMasterView = true;
      this.user.viewuserMasterView = true;
      this.user.modifyuserMasterView = true;
      this.user.deleteuserMasterView = true;

    }
    if (!value) {
      this.user.adduserMasterView = false;
      this.user.viewuserMasterView = false;
      this.user.modifyuserMasterView = false;
      this.user.deleteuserMasterView = false;
    }

  }



  isallowMobileMasterChecked() {
    //  ;
    ////console.log("####isuserMasterChecked######", this.user.allowMobileMasterAllView);

    this.allowMobileMasterselectAll(this.user.allowMobileMasterAllView);
  }
  allowMobileMasterselectAll(value: boolean) {
    if (value) {
      this.user.addmobileMasterView = true;
      this.user.viewmobileMasterView = true;
      this.user.modifymobileMasterView = true;
      this.user.deletemobileMasterView = true;
    }
    else {
      this.user.addmobileMasterView = false;
      this.user.viewmobileMasterView = false;
      this.user.modifymobileMasterView = false;
      this.user.deletemobileMasterView = false;
    }
  }
  islocationChecked(){
    ////console.log("####islocationChecked####" , this.user.locationAllView);
    this.locationselectAll(this.user.locationAllView);
  }
  locationselectAll(value:boolean){
    if(value){
      this.user.addlocationView=true;
      this.user.viewlocationView=true;
      this.user.modifylocationView=true;
      this.user.deletelocationView=true;
    }
    else{
      this.user.addlocationView=false;
      this.user.viewlocationView=false;
      this.user.modifylocationView=false;
      this.user.deletelocationView=false;
    }
  }


  isdepartmentChecked(){
    ////console.log("####isdepartmentChecked####" , this.user.departmentAllView);
    this.departmentselectAll(this.user.departmentAllView);
  }
  departmentselectAll(value:boolean){
    if(value){
      this.user.adddepartmentView=true;
      this.user.viewdepartmentView=true;
      this.user.modifydepartmentView=true;
      this.user.deletedepartmentView=true;
    }
    else{
      this.user.adddepartmentView=false;
      this.user.viewdepartmentView=false;
      this.user.modifydepartmentView=false;
      this.user.deletedepartmentView=false;
    }
  }

  isphoneDirectoryChecked() {
    ////console.log("####isphoneDirectoryChecked######", this.user.phoneDirectoryAllView);
    this.phoneDirectoryselectAll(this.user.phoneDirectoryAllView);
  }
  phoneDirectoryselectAll(value: boolean) {
    if (value) {
      this.user.addphoneDirectoryView = true;
      this.user.viewphoneDirectoryView = true;
      this.user.modifyphoneDirectoryView = true;
      this.user.deletephoneDirectoryView = true;
    }
    if (!value) {
      this.user.addphoneDirectoryView = false;
      this.user.viewphoneDirectoryView = false;
      this.user.modifyphoneDirectoryView = false;
      this.user.deletephoneDirectoryView = false;
    }
  }



  iscontactGroupChecked() {
    ////console.log("####iscontactGroupChecked######", this.user.contactGroupAllView);
    this.contactGroupselectAll(this.user.contactGroupAllView);
  }
  contactGroupselectAll(value: boolean) {
    if (value) {
      this.user.addcontactGroupView = true;
      this.user.viewcontactGroupView = true;
      this.user.modifycontactGroupView = true;
      this.user.deletecontactGroupView = true;
    }
    if (!value) {
      this.user.addcontactGroupView = false;
      this.user.viewcontactGroupView = false;
      this.user.modifycontactGroupView = false;
      this.user.deletecontactGroupView = false;
    }
  }


  issmsDraftChecked() {
    ////console.log("####issmsDraftChecked######", this.user.smsDraftAllView);
    this.smsDraftselectAll(this.user.smsDraftAllView);
  }
  smsDraftselectAll(value: boolean) {
    if (value) {
      this.user.addsmsDraftView = true;
      this.user.viewsmsDraftView = true;
      this.user.modifysmsDraftView = true;
      this.user.deletesmsDraftView = true;
    }
    if (!value) {
      this.user.addsmsDraftView = false;
      this.user.viewsmsDraftView = false;
      this.user.modifysmsDraftView = false;
      this.user.deletesmsDraftView = false;
    }
  }


  isinstantMessageChecked() {
    ////console.log("####isinstantMessageChecked######", this.user.instantMessageAllView);
    this.instantMessageselectAll(this.user.instantMessageAllView);
  }
  instantMessageselectAll(value: boolean) {
    if (value) {
      this.user.addinstantMessageView = true;

    }
    if (!value) {
      this.user.addinstantMessageView = false;
    }
  }


  isinstantVoiceChecked() {
    ////console.log("####isinstantVoiceChecked######", this.user.instantVoiceAllView);

  }

  isscheduleMessageChecked() {
    ////console.log("####isscheduleMessageChecked######", this.user.scheduleMessageAllView);
    this.scheduleMessageselectAll(this.user.scheduleMessageAllView);
  }
  scheduleMessageselectAll(value: boolean) {
    if (value) {
      this.user.addscheduleMessageView = true;
      this.user.viewscheduleMessageView = true;
      this.user.modifyscheduleMessageView = true;
      this.user.deletescheduleMessageView = true;
    }
    if (!value) {
      this.user.addscheduleMessageView = false;
      this.user.viewscheduleMessageView = false;
      this.user.modifyscheduleMessageView = false;
      this.user.deletescheduleMessageView = false;
    }
  }

  isscheduleVoiceChecked() {
    ////console.log("####isscheduleVoiceChecked######", this.user.scheduleVoiceAllView);

  }

  isinstantMessageReportChecked() {
    ////console.log("####isinstantMessageReportChecked######", this.user.instantMessageReportAllView);
    this.instantMessageReportselectAll(this.user.instantMessageReportAllView);
  }
  instantMessageReportselectAll(value: boolean) {
    if (value) {
      this.user.viewinstantMessageReportView = true;
      this.user.modifyinstantMessageReportView=true;
      this.user.deleteinstantMessageReportView=true;

    }
    if (!value) {
      this.user.viewinstantMessageReportView = false;
      this.user.modifyinstantMessageReportView=false;
      this.user.deleteinstantMessageReportView=false;
    }
  }


  isinstantvoiceReportChecked() {
    ////console.log("####isinstantvoiceReportChecked######", this.user.instantvoiceReportAllView);
    this.instantvoiceReportselectAll(this.user.instantvoiceReportAllView);

  }
  instantvoiceReportselectAll(value: boolean) {
    if (value) {
      this.user.viewinstantvoiceReportView = true;

    }
    if (!value) {
      this.user.viewinstantvoiceReportView = false;

    }
  }


  isscheduleMessageReportChecked() {
    ////console.log("####isscheduleMessageReportChecked######", this.user.scheduleMessageReportAllView);
    this.scheduleMessageReportselectAll(this.user.scheduleMessageReportAllView);
  }
  scheduleMessageReportselectAll(value: boolean) {
    if (value) {
      this.user.viewscheduleMessageReportView = true;

    }
    if (!value) {
      this.user.viewscheduleMessageReportView = false;

    }
  }


  isscheduleVoiceReportChecked() {
    ////console.log("####isscheduleVoiceReportChecked######", this.user.scheduleVoiceReportAllView);
    this.scheduleVoiceReportselectAll(this.user.scheduleVoiceReportAllView);
  }
  scheduleVoiceReportselectAll(value: boolean) {
    if (value) {
      this.user.viewscheduleVoiceReportView = true;

    }
    if (!value) {
      this.user.viewscheduleVoiceReportView = false;

    }
  }



  getLocationlist() {
    this.getService.getLocationList().subscribe((locationList) => {
      this.locationList = locationList;
      this.itemDataServiceService.setLocationList(this.locationList)

      ////console.log(locationList);
      if(locationList.length == 0)
      {
      
        
         
    }
    }, (error) => {
      ////console.log(error);
    }
    );
  }

 

  getuserList() {
    this.app.checkCredential();
    this.getService.getUserList().subscribe((userList) => {
      this.userList = userList;
      this.numberOfData = this.userList.length;
      this.limit = this.userList.length;
      ////console.log(this.userList);
    })
  }



  //validation

  checkcontactExist(){
    
    this.isContactExsit=false;
    for (let i in this.userList) {
      if (this.userList[i].mobile == this.user.mobile) {
        this.isContactExsit = true;
        this.contactMessage = "already exists!";
        ////console.log("Contact already exixts")
        break;
      }
      else 
      {
        this.isContactExsit=false;
      }
    }
  

  }


  checkValidation()
{
  this.checkUserLocationValid();
  this.checkUserTypeValid();
  this.checkUserDeparmentValue();
}

  checkUserTypeValid(){
    
    this.accessHide(this.user.userType);
    if(this.user.userType == 0)
    {
      this.userRequired = true;
      this.isUserTypeValid=true;
       return this.userRequired;
    }else{
      this.userRequired = false;
      this.isUserTypeValid=false;
    }
  }

  accessHide(userType){
    this.accessLocation=true;
      this.accessDeparment=true;
      this.accessUser=true;
      this.accessMobileMaster=true;
    switch(userType){
      case "superadmin":
      this.accessLocation=true;
      this.accessDeparment=false;
      this.accessUser=true;
    
      break;
      case "admin":
      this.accessLocation=false;
      this.accessDeparment=true;
      this.accessUser=true;
     
      break;
      case "user":
      this.accessLocation=false;
      this.accessDeparment=false;
      this.accessUser=false;
      
      break;

    }
  }


  checkUserLocationValid(){
   
      
       if (this.location1.locationName1sss == 0) {
      
      this.locationRequired = true;
      this.isLocationValid=true;
      this.locationMessage = "Please select location";
      return this.locationRequired;
    }
    else
    {
      this.isLocationValid=false;
      this.locationRequired = false;
    }
  }


  checkUserDeparmentValue(){
   
    
    if (this.department1.departmentName1 == 0) {
       this.isDepartmentValid=true;
       this.departmentRequired=true;
      this.departmentMessage = "Please select department";
      return this.isDepartmentValid;
    }
    else
    {
      this.departmentRequired=false;
      this.isDepartmentValid=false;
      
    }
  }





  saveUser() {
    ////console.log("create ", this.loginId.loginId)
    this.user.createDate = new Date();
    if(this.loginId.userType=="superadmin"){
      this.userRequired = true;
      this.isUserTypeValid=true;
       if (this.location1.locationName1 == 0) {
        this.isLocationValid=true;
      this.locationRequired = true;
      this.locationMessage = "Please select location";
      return  this.isLocationValid;
    }
    else
    {
      this.isLocationValid=false;
      this.locationRequired = false;
    }
    this.locationRequired = true;
    this.isLocationValid=true;
  if(this.user.userType == 0)
  {
    this.userRequired = true;
    this.isUserTypeValid=true;
     return this.userRequired;
  }else{
    this.userRequired = false;
    this.isUserTypeValid=false;
  }

      if(this.location1.locationName1 != 0 && this.user.userType != 0){
        this.user.locationName = this.location1.locationName1;
          this.isfullfill=true;
      }else{
        this.isfullfill=false;
        ////console.log("not submit")
      }
      
    }else if(this.loginId.userType=="admin"){
        this.userRequired = true;
        this.isUserTypeValid=true;
        this.location1.locationName1=this.loginId.locationName;
  if (this.department1.departmentName1 == 0) {
     this.isDepartmentValid=true;
     this.departmentRequired=true;
    
    this.departmentMessage = "Please select department";
    return this.isDepartmentValid;
  }
  else
  {
    this.departmentRequired=false;
    this.isDepartmentValid=false;
    this.locationRequired = false;
  }
  this.locationRequired = true;
  this.isLocationValid=true;
if(this.user.userType == 0)
{
  this.userRequired = true;
  this.isUserTypeValid=true;
   return this.userRequired;
}else{
  this.userRequired = false;
  this.isUserTypeValid=false;
}

      if(this.department1.departmentName1 != 0 && this.user.userType != 0){
        this.user.locationName = this.location1.locationName1
        this.user.departmentName=this.department1.departmentName1;
        this.isfullfill=true;
      }else{
        this.isfullfill=false;
        ////console.log("not submit")
      }
      
    }
       
    this.user.createdBy = this.loginId.loginId;

    if(!this.isfullfill){
      alert("sorry");
        ////console.log("not submit not fullfill ");
    }else if ((this.user.userName != undefined && this.user.locationName != 0)||(this.user.userName != undefined && this.user.departmentName != 0)) {

      if (this.user.password == this.user.confPassword) {
        this.spinner.show();
        this.postService.saveUser(this.user).map((response) => response)
          .subscribe(data => {
            this.getuserList();
            if (data.status == 201) {
              ////console.log("save data" + data);
              this.user = this.emptyUser;
                this.spinner.hide();
                this.successmsg = true;
                 this.empty();
            }
            setTimeout(() => {
              this.successmsg = false;
            }, 2000);

          }, error => {
            ////console.log("error ", error);
            this.user = this.emptyUser;
            setTimeout(() => {
              this.spinner.hide();
              this.errormsg = true;
            }, 2000)
            setTimeout(() => {
              this.errormsg = false;
            }, 4000)
          })
      } else {
        setTimeout(() => {
          this.spinner.hide();
          this.errorpassword = true;
        }, 2000)
        setTimeout(() => {
          this.errorpassword = false;
        }, 4000)
      }
    }

  }

  saveUpdateUser(){
    
        this.user.createDate = new Date();
        if(this.loginId.userType=="superadmin"){
          this.userRequired = true;
          this.isUserTypeValid=true;
           if (this.location1.locationName1== 0) {
            this.isLocationValid=true;
          this.locationRequired = true;
          this.locationMessage = "Please select location";
          return  this.isLocationValid;
        }
        else
        {
          this.isLocationValid=false;
          this.locationRequired = false;
        }
        this.locationRequired = true;
        this.isLocationValid=true;
      if(this.user.userType == 0)
      {
        this.userRequired = true;
        this.isUserTypeValid=true;
         return this.userRequired;
      }else{
        this.userRequired = false;
        this.isUserTypeValid=false;
      }
    
          if(this.location1.locationName1 != 0 && this.user.userType != 0){
            this.user.locationName = this.location1.locationName1;
              this.isfullfill=true;
          }else{
            this.isfullfill=false;
            ////console.log("not submit")
          }
        }else if(this.loginId.userType=="admin"){
          this.userRequired = true;
        this.isUserTypeValid=true;
  if (this.department1.departmentName1 == 0) {
     this.isDepartmentValid=true;
     this.departmentRequired=true;
    this.departmentMessage = "Please select department";
    return this.isDepartmentValid;
  }
  else
  {
    this.departmentRequired=false;
    this.isDepartmentValid=false;
    this.locationRequired = false;
  }
  this.locationRequired = true;
  this.isLocationValid=true;
if(this.user.userType == 0)
{
  this.userRequired = true;
  this.isUserTypeValid=true;
   return this.userRequired;
}else{
  this.userRequired = false;
  this.isUserTypeValid=false;
} if(this.department1.departmentName1 != 0 && this.user.userType != 0){
          this.user.locationName = this.location1.locationName1
          this.user.departmentName=this.department.departmentName1;
          this.isfullfill=true;
      }else{
        this.isfullfill=false;
        ////console.log("not submit")
      }
      
    } 
        this.user.createdBy = this.loginId.loginId;
        if(!this.isfullfill){
          alert("sorry");
            ////console.log("not submit not fullfill ");
        }else if ((this.user.userName != undefined && this.user.locationName != 0)||(this.user.userName != undefined && this.user.departmentName != 0)) {
                            
          this.postService.updateUser(this.user).map((response) => response)
              .subscribe(data => {
                this.getuserList();
                //////console.log(data);
                if (data.status == 200) {
                  //this.user = this.emptyUser;
                    this.spinner.hide();
                    this.updatesuccessmsg = true;
                }
                setTimeout(() => {
                  this.updatesuccessmsg = false;
                  this.showUserList();
                }, 2000);
    
              }, error => {
                //////console.log("error ", error);
                this.user = this.emptyUser;
                setTimeout(() => {
                  this.spinner.hide();
                  this.errormsg = true;
                }, 2000)
                setTimeout(() => {
                  this.errormsg = false;
                }, 4000)
              })
        }
    }
      
         





  saveUpdateUser11() {
    
    this.locationRequired = false;
    
    this.userRequired = false;
    if (this.location.locationName == 0) {
      this.locationRequired = true;
      this.locationMessage = "Please select location";
      return this.locationRequired;
    }
    else
    {
      this.locationRequired = false;
    }
   
    if(this.user.userType == 0)
    {
      this.userRequired = true;
      this.userMessage = "Please select user";
      return this.userRequired;
    }

    else
    {
      this.userRequired = false;
    }

  this.spinner.show();

   //////console.log("key ")
    //this.loginId= this.itemDataServiceService.getUser();
    //////console.log("create ", this.loginId.loginId)


    for (let i in this.locationList) {
      if (this.location.locationName == this.locationList[i].locationName) {
        this.location = this.locationList[i];
       // ////console.log(location)
      }
    }

    this.user.createDate = new Date();
    this.user.location = this.location;
    this.user.createdBy = this.loginId.loginId;

    //  ////console.log("locate i"+this.location);

    // ////console.log(" user =>"+this.user.toString());

    if (this.user.userName != undefined && this.user.location != undefined) {

      //     if(this.user.password == this.user.confPassword){
      this.postService.updateUser(this.user).map((response) => response)
        .subscribe(data => {
          //////console.log("update data" + data.status);
          
          if (data.status == 200) {
            // setTimeout(() => {
              this.spinner.hide();
            this.updatesuccessmsg = true;
            this.authenticationError = false;
            this.user = this.emptyUser;
          // }, 2000)
        }
        setTimeout(() => {
          this.updatesuccessmsg = false;
          this.showUserList();
        }, 4000);
       
        
        this.getuserList();
      }, error => {
        setTimeout(() => {
          this.spinner.hide();
          this.updatesuccessmsg = false;
          this.errormsg = true;
          this.authenticationError = true;
         }, 2000);
       
        setTimeout(() => {
          this.errormsg = false;
          this.authenticationError = false;
        }, 4000)
      })
      // }else{
      //      this.errorpassword=true;
      //   setTimeout(()=>{
      //     this.errorpassword=false;
      //   },3000)
      //   }
    }

  }





  // updateUser1(){
  //        this.user.createDate =new Date();
  //       this.postService.updateUser(this.user).map((response)=>response)
  //       .subscribe(data =>{
  //         if(data.status==200){
  //           this.updatesuccessmsg=true;
  //           setTimeout(()=>{
  //               this.updatesuccessmsg=false;
  //           },5000);
  //         }
  //         ////console.log("update data"+data.status);
  //      //   alert("SUCCESS");
  //         this.getuserList();
  //       })
  // }

  deleteUser(user) {
   // this.spinner.show();
    this.getService.deleteUser(user.loginId).map((response) => response)
      .subscribe(data => {
        //////console.log("update data" + data.status);
        setTimeout(() => {
          this.closeModel(); 
        }, 500);
        setTimeout(() => {
          //this.spinner.hide();
          this.deletesuccessmsg = true;
          this.authenticationError = false;
        }, 500);

        this.getuserList();
        setTimeout(() => {
          this.deletesuccessmsg = false;
        }, 4000);

      },
        error => {
          setTimeout(() => {
           // this.spinner.hide();
            this.deletesuccessmsg = false;
            this.authenticationError = true;

          }, 2000);


          setTimeout(() => {
            this.authenticationError = false;
          }, 4000);
        });
  }

  // hide list when user update click
  hideUserList() {

    this.user
    this.USERSREPORT = false;
    this.locationRequired = false;
    this.userRequired = false;  
    this.contactExists = false;
  // this.empty();
  }
  showUserList() {
    this.updateON = false;
    this.USERSREPORT = true;
    this.locationRequired = false;
    this.userRequired = false;
    this.contactExists = false;
  // this.empty();
    
   }




  showUserDetails(user) {
    // this.userForm.disabled;
    this.disabled = true;
    this.user = user;
    this.user.confPassword = this.user.password;
    // this.location = this.user.location;
    // user.locationName = this.user.location.locationName;
    ////console.log("location "+this.user.locationName);
    for(let i in this.locationList){
      if(this.user.locationName==this.locationList[i].locationName)
      {
        this.location=this.locationList[i];
         ////console.log("user location "+this.location);
        break;
      }
    }

    for (let i in this.departmentList) {
      
      if (this.user.departmentName == this.departmentList[i].departmentName) {
        this.department= this.departmentList[i];
        ////console.log("department   ",this.department)
      }
    }

    this.userDetailsShow = true;
    ////console.log(user);
    this.hideUserList();

  }

  //hide when details hide
  hideUserDetails() {
    this.user = this.emptyUser;
    this.disabled = false;
    this.showUserList();
    this.updateON = false;
    this.userDetailsShow = false;
  }


  OpenModel(user, updateModal) {
   
    this.user = user;
    if (this.user.userType=="user") {

      this.accessLocation=false;
      this.accessDeparment=false;
      this.accessUser=false;
      
    }

    else if(this.user.userType=="admin"){
      this.accessLocation=false;
     this.accessDeparment=true;
     this.accessUser=true;
    }
     else if(this.user.userType=="superadmin"){
     this.accessLocation=true;
      this.accessDeparment=false;
     this.accessUser=true;
    }

    this.userDetailsShow = true;
    this.user.confPassword = this.user.password;

     ////console.log("location "+this.user.locationName);
     for(let i in this.locationList){
      if(this.user.locationName==this.locationList[i].locationName)
       {
         this.location1=this.locationList[i];
         this.location1.locationName1=this.locationList[i].locationName;
          ////console.log("user location "+this.location);
         break;
       }
     }
     
    
    ////console.log("deparment ", user.departmentName)
    for (let i in this.departmentList) {
      
      if (this.user.departmentName == this.departmentList[i].departmentName) {
        this.department1= this.departmentList[i];
        this.department1.departmentName1=this.departmentList[i].departmentName;
        ////console.log("user department    ",this.department)
      }
    }
    this.disabled = false;
    this.updateON = true;
    ////console.log(user);
    this.hideUserList();
    //  this.myModel=this.modelService.open(updateModal);

  }
  // closeModel(){
  //   this.myModel.close();
  //    ////console.log(location);


  // }

  restrictNumeric(event) {
    return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57;
  }


  // checkValidation() {
  //   this.locationRequired = false;
  //   if (this.location.locationName == 0) {
  //     this.locationRequired = true;
  //     this.locationMessage = "Please Select location";
  //     return this.locationRequired;
  //   }
  //   else
  //   {
  //     this.locationRequired = false;
  //   }
  //   this.userRequired = false;
  //   if( this.user.userType == 0){
  //     this.userRequired = true;
  //     this.userMessage = "Please select user";
  //     return this.userRequired;
  //   }
  //   else
  //   {
  //     this.userRequired = false;
  //   }
  // }

  // mobileValidation()
  // {
    
  //   this.contactExists = false;
  //   for (let i in this.userList) {

  //     if (this.userList[i].mobile == this.user.mobile) {
  //       this.contactExists = true;
        
  //       ////console.log("Contact already exixts")
  //       break;

  //     }
  //     else 
  //     {
  //       this.contactExists = false;
  //     }
  //   }
  //   if(this.contactExists && !this.updateON){
  //     this.contactMessage = "already exists!";
  //   }else{
  //     this.contactExists = false;
  //   }

  // }
  
  Confirm()
  {
    this.router.navigate(['admin/location/master']);
    // alert('You cliked confirm');
  }

  getDepartmentList(){
    this.getService.getDepartmentList().subscribe(data=>{
      ////console.log("department list ",data);
      this.departmentList=data;
    })
  }



  checkUserType(userType){
    switch(userType){
      case "superadmin":
        this.isSuperAdmin=true;
        this.isAdmin=true;
        this.isUser=true;
      break;

      case "admin":
      this.isSuperAdmin=false;
      this.isAdmin=false;
      this.isUser=true;
      break;
    
    }

  }

  open(user,deleteModal) {
   
    this.user = user;
   ////console.log("***********************" ,sms);
     this.myModel = this.modelService.open(deleteModal ,  { windowClass: 'custom-class ' });
  }

  
  closeModel()
  {
    this.getuserList();
    this.myModel.close();
    this.locationRequired = false;
    this.userRequired = false;
    this.contactExists = false;
    this.user = this.emptyUser;
  }

  // checkMobileNumber() {
  //   this.contactExists = false;
  //   ////console.log(this.contact.mobileNo1.length);
   
  //   if(this.user.mobile.length==10){
   
  //     ////console.log(this.contact.mobileNo1);
  //     this.getService.getAllUserMobileList(this.user.mobile)
  //     .subscribe((data) => {
  //     this.mobileList=data;
  //     if(this.mobileList!=null){
  //       this.contactExists = true;
  //       this.contactMessage = "already exists!"
  //     }else{
  //       this.contactExists = false;
  //     }
  //      ////console.log(data);
  //   });
  //   }
    
   
  //   }

    empty()
    {
      this.user.userName='';
      this.user.loginId='';
      this.user.mobile='';
      this.user.password='';
      this.user.emailId='';
      this.user.confPassword='';
    }
}
