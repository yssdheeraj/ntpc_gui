import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { GetService } from '../../services//get.service';
import { PostService } from '../../services/post.service';
import{Location} from '../../shared/location';
import { DatePipe } from '@angular/common';
import {NgbModal,ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { errorHandler } from '@angular/platform-browser/src/browser';
import{User} from '../../shared/user';
import { error } from 'selenium-webdriver';
import { NgxSpinnerService } from 'ngx-spinner';
import {FormsModule,FormGroup,FormControl,Validators} from '@angular/forms';

import { AppComponent } from '../../app.component';



@Component({
   selector: 'app-location-master',
  templateUrl: './location-master.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./location-master.component.css']
})
export class LocationMasterComponent implements OnInit {


  locationList1 : any=[];
  location:any={};
  // data:any='';
  // time:any='';
  public loginId:any={};
  newDate:any;
  myModel:any;
  result1:boolean=false;
  result2:any;
  public successField: any = false;
  public authenticationError:boolean;
  public successEdit: boolean;
  public deleteField: boolean;
  public nolocationCreated:boolean;
  public locationExists:boolean;
  public locationMessage:string;
  public updateON: boolean = false;

    name = 'Angular v4 - Applying filters to *ngFor using pipes';
    numberOfData: number;
    limit: number;
    page: number = 1;
    filter:any={};
  constructor(private getService:GetService,private modelService:NgbModal,
              private postService:PostService,private itemDataServiceService:ItemDataServiceService,
              private spinner: NgxSpinnerService,private app:AppComponent) { }

  ngOnInit() {
  
    this.loginId=this.itemDataServiceService.getUser();
    this.getLocationlist();
    
     }

     
     saveLocation(){
      if(this.location.locationName!=undefined)
      {
       this.spinner.show();
           this.postService.saveLocation(this.location).map((response) =>response)
        .subscribe(data=>{
          this.getLocationlist();
    
           // //console.log("Location save successfully",data);
            if(data.status==201){
            
              this.spinner.hide();
              this.successField=true;
              this.authenticationError=false;
              this.location.locationId='';
              this.location.locationName='';
            

          }
            
            setTimeout(()=>{
                  this.successField=false;
                  //this.closeModel();
            },2000 );
        },error=>{
          setTimeout(() => {
            this.spinner.hide();
            this.successField = false;
            this.authenticationError = true;
           
          }, 2000);
          setTimeout(() => {
            this.authenticationError = false;
            //this.closeModel();
          }, 4000);
        })
     }
 
    }
     

  getLocationlist(){
    this.app.checkCredential();
    this.getService.getLocationList().subscribe((locationList)=>{
      this.locationList1=locationList;
      ////console.log(locationList);
      this.numberOfData = this.locationList1.length;
        this.limit = this.locationList1.length;
      if (this.locationList1 < 1) {
        this.nolocationCreated = true;
      }
      else{
        this.nolocationCreated = false;
      }
     },
       
  (error)=>{
      ////console.log(error);
    }
  );
}


updateLocation(){
 
  this.spinner.show();
  this.location.createDate= new Date();
   
    ////console.log(this.location)
     if(this.location.locationName!='' && this.location.locationName!=''){
      this.postService.updateLocation(this.location).map((response)=>response)
      .subscribe(data=>{
        this.getLocationlist();
        ////console.log(data);
        if(data.status == 201){
         
            this.spinner.hide();
            this.successEdit = true;
            this.authenticationError = false;
            this.location.locationId='';
            this.location.locationName='';
        
        }
        setTimeout(() => {
          this.successEdit = false;
          this. closeModel();
        }, 2000);
       
        },
        error => {
          
          setTimeout(() => {
            this.spinner.hide();
            this.successEdit = false;
            this.authenticationError = true;
            
           }, 2000);
          
  
          setTimeout(() => {
            this.authenticationError = false;
            this.closeModel();
          }, 4000);
           
        });
       }
     
    
 }



 deleteLocation(location){
   //this.spinner.show();
  this.deleteField = false;
    this.location=location;
    ////console.log(this.location)
     if(this.location.locationName!='' && this.location.locationName!=''){
       this.getService.deleteLocation1(this.location.locationId).map((response)=>response)
      .subscribe(data=>{
       
        ////console.log(data);
        setTimeout(() => {
          this.closeModel();
        }, 500);
        setTimeout(() => {
          //this.spinner.hide();
          this.deleteField = true;
          this.authenticationError = false;
        }, 500);
        this.getLocationlist();
        setTimeout(() => {
          this.deleteField = false;
        }, 4000);
       
      },
      error => {
        setTimeout(() => {
          //this.spinner.hide();
          this.deleteField = false;
          this.authenticationError = true;
          
         }, 2000);
        
         setTimeout(() => {
          this.authenticationError = false;
        }, 4000);
      });
      
  }
}


 OpenModel(location,updateModal){
   this.locationExists=false;
   this.updateON = true;
    this.successEdit = false;
    this.location=location;
     ////console.log(location);
    this.myModel=this.modelService.open(updateModal , { windowClass: 'custom-class' });
  }
  
  closeModel(){
    this.getLocationlist();
    this.locationExists=false;
    this.location.locationId='';
    this.location.locationName='';
    this.myModel.close();
     ////console.log(location);
   
  }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);
 
  // checkLocation()
  // {
  //   this.locationExists = false;
  // for (let i in this.locationList1) {

  //   if (this.locationList1[i].locationName == this.location.locationName) {
  //     this.locationExists = true;
  //     this.locationMessage = "Already exists!";
  //     break;
  //   }
  //   else {
  //     this.locationExists = false;
  //   }
  // }  if(this.locationExists && !this.updateON){
  //   this.locationMessage = "Already exists!";
  // }else{
  //   this.locationExists = false;
  // }
  // }

  open(location,deleteModal) {
    this.locationExists=false;
    this.location = location;
   ////console.log("***********************" ,sms);
     this.myModel = this.modelService.open(deleteModal ,  { windowClass: 'custom-class ' });
  }

}
