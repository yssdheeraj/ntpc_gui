import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { GetService } from '../../services//get.service';
import { PostService } from '../../services/post.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { error } from 'util';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppComponent } from '../../app.component';




@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  
  public isSuperAdmin:boolean=false;
  public isAdmin:boolean=false;
  public isUser:boolean=false;
  public userList:any[];
  public filterUserList:any=[];
  public user :any = {};
  public user1 :any = {};
  public checkResult:boolean=false;
  public result:boolean=false;
  public checkSuccesResult:boolean=false;
  public checkErrorResult:boolean=false;
  public statusCode: number;
  public admintrue:any=false;
  public usertrue:any=false;

  public oldPassword:any='';
  public loginId:any ='';
  public password ='';
  public confirmPassword ='';
  constructor(private router:Router,private itemDataServiceService:ItemDataServiceService,
    private getService:GetService,private postService:PostService,
    private modelService:NgbModal,private spinner: NgxSpinnerService,private app:AppComponent) { 
      this.user.loginId = 0;
    }

  ngOnInit() {
    this.spinner.show();
    this.loginId=this.itemDataServiceService.getUser();
    ////console.log("Check CURRENTLY LOGIN" ,this.loginId.loginId);
    this.checkUserType(this.loginId.userType);
    this.getuserList();
    this.user1=this.loginId;
    setTimeout(() =>{
      this.spinner.hide();
    },2000);
    
  }


  getuserList() {
    this.app.checkCredential();
    this.getService.getUserList().subscribe((userList) => {
      this.userList = userList;
     ////console.log(this.userList);
    })
  }


  getFilterUserList(){
    if(this.loginId.userType=='admin'){
    this.usertrue=false;
      this.admintrue=true;
      for(let i in this.userList){
        if(this.userList[i].createdBy==this.loginId.loginId){
            ////console.log(this.userList[i]);
            this.filterUserList.push(this.userList[i]);
        }
        else if(this.userList[i].loginId==this.loginId.loginId){
          this.filterUserList.push(this.userList[i]);
        }
      }
    }
    if(this.loginId.userType =='user')
    {
      this.admintrue=false;
      this.usertrue=true;
    }
    
  }

  // resetPassword()
  // {
  //   ////console.log("ddddddddddddddddd" ,this.user);
  //   if(this.user.confirmPassword  == this.user.password){
  //     ////console.log("ddddddd" ,this.user);
  //     this.postService.changeAdminPassword(this.user).map(
  //       (response) => response
  //     ).subscribe(data =>{
  //       ////console.log(data);
  //        if(data.status==200){
  //         this.checkSuccesResult=true;

  //        }
  //        setTimeout(() =>{
  //          this.user={};
  //           this.checkSuccesResult=true;
  //           this.logout();
  //        },3000);
  //       },error=>{
  //           ////console.log(error);
  //       } 
  //     );
  //   }
  //   else{
  //   this.checkErrorResult=true;
  //   setTimeout(() =>{
  //     this.checkErrorResult=false;
  //  },5000);
  //   }
      
//  }

 resetPassword()
  {
   
    ////console.log("ddddddddddddddddd" ,this.user);
    if(this.user.confirmPassword  == this.user.password){
      ////console.log("ddddddd" ,this.user);
      this.postService.changeAdminPassword(this.user).map(
        (response) => response
      ).subscribe(data =>{
        //////console.log("******************************" ,data);
         if(data.status==200){
          this.checkSuccesResult=true;
            if (this.user.loginId == this.loginId.loginId) {
              
              setTimeout(() =>{
                this.user={};
                 this.checkSuccesResult=false;
                 this.logout();
              },3000);
            } else {
              
              setTimeout(() =>{
                this.user={};
                 this.checkSuccesResult=false;
              },3000);
            }
         }

        //  setTimeout(() =>{
        //    this.user={};
        //     this.checkSuccesResult=true;
        //     //this.logout();
        //  },3000);
        },error=>{
            ////console.log(error);
        } 
      );
    }
    else{
    this.checkErrorResult=true;
    setTimeout(() =>{
      this.checkErrorResult=false;
   },5000);
    }
      
 }

    


  checkPassword(){
    if(this.user.confirmPassword  == this.user.password){
    this.user.loginId=this.loginId.loginId;
    this.postService.changeUserPassword(this.user).map((response) => response)
    .subscribe(data =>{
      if(data.status==200){
        this.checkSuccesResult=true;
        this.user={};
        setTimeout(()=>{
          this.logout();
        },3000);
      }else{
        this.checkResult=true;
      }     
      },error=>{
          ////console.log(error);
      } 
    );
  }else{
    this.checkErrorResult=true;
  }
  setTimeout(() =>{
     this.checkSuccesResult=false;
     this.checkResult=false;
     this.checkErrorResult=false;
  },3000);

  }

  logout(){
    this.postService.doLogout(this.loginId).map((response=>response))
    .subscribe(data=>{
      ////console.log("log out successfull "+data);
      localStorage.clear();
     this.router.navigate(['']);
    },error =>{
      ////console.log("log out error "+error);
    })
    this.loginId={};
    this.router.navigate(['']);
  }

  checkUserType(userType){
  
    switch(userType){
      case "superadmin":
        this.isSuperAdmin=true;
        this.isAdmin=true;
       
      break;

      case "admin":
      this.isSuperAdmin=true;
      this.isAdmin=true;
     
      break;
     case "user":
     this.isSuperAdmin=false;
     this.isAdmin=false;
     this.isUser=true;
     break;
    }

  }

}
