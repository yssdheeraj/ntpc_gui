import { Component, OnInit, Type,ViewChild } from '@angular/core';
import { GetService } from '../services//get.service';
import { PostService } from '../services/post.service';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemDataServiceService } from '../services/item-data-service.service';
import { AppComponent } from '../app.component';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgModel } from '@angular/forms/src/directives/ng_model';
import { RouterModule, Router } from '@angular/router';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';


@Component({
  selector: 'app-message-detail',
  templateUrl: './message-detail.component.html',
  styleUrls: ['./message-detail.component.css']
})
export class MessageDetailComponent implements OnInit {


  name = 'Angular v4 - Applying filters to *ngFor using pipes';
  numberOfData: number;
  limit: number;
  page: number = 1;
  filter:any={};
  public messageDetail: any = [];
  public smsDetail: any = {};
  public successField: any = false;
  public successEdit: boolean;
  public authenticationError: boolean;
  public authenticationError2: boolean;
  public deleteField: boolean;
  public instant: any = {
    mobile: '',
    smsName: '',
    smsText: '',
    location:'',
    phoneDirectoryName: '',
    contactGroupName: '',
    departmentName: '',
    priority: '',
    scheduleDate: '',
    scheduleType: '',
    uploadContactList:''

  };
  public sms: any = {};
  public strDate:any;
  public loginId:any;
  public uploadContactList:any;
  //public duplicatList: any = [];
  public scheduleType: any;
  public message:any;
  public scheduleDate:any;
  public scheduleTime:any;
  public cancleField:any=false;

  myModel: any;
public phone:any={};

  constructor(private itemDataServiceService: ItemDataServiceService, private getService: GetService, private postService: PostService,
    private spinner: NgxSpinnerService, private app: AppComponent,private router:Router, private modelService: NgbModal,) { 

     
       
    }

  ngOnInit() {
    this.spinner.show();
    this.smsDetail = this.itemDataServiceService.getMessageData();
    this.loginId = this.itemDataServiceService.getUser();
    this.saveInstaceMessage();
 
   
  }

  public scheduleTypes: any = [

    { id: 0, scheduleType: "ONETIME", value: "ONETIME" },
    { id: 1, scheduleType: "DAILY", value: "DAILY" },
    { id: 2, scheduleType: "WEEKLY", value: "WEEKLY" },
    { id: 3, scheduleType: "MONTHLY", value: "MONTHLY" },
    { id: 4, scheduleType: "YEARLY", value: "YEARLY" }
  ]

  public smsPriorities: any = [


    { id: 0, priority: "MEDIUM", value: "MEDIUM" },
    { id: 1, priority: "LOW", value: "LOW" },
    { id: 2, priority: "HIGH", value: "HIGH" }

  ]

  saveInstaceMessage() {
  
    this.instant.locationName
    this.setDateFormat(this.smsDetail.scheduleDate);
    this.instant.scheduleDate=this.strDate;
    this.instant.mobile = this.smsDetail.mobile;
    this.instant.smsName = this.smsDetail.messageName;
    this.instant.smsText = this.smsDetail.message;
    this.instant.phoneDirectoryName = this.smsDetail.phoneDirectory;
    this.instant.contactGroupName = this.smsDetail.contactGroup;
    this.instant.departmentName = this.smsDetail.departments;
    this.instant.locationName = this.smsDetail.locations;
    this.instant.projectName = this.smsDetail.projectName; 
    this.instant.grade = this.smsDetail.grades; 
    this.instant.substGradeName = this.smsDetail.substGrades;
    
    
    this.instant.scheduleType = this.smsDetail.scheduleType;
    this.instant.loginId = this.loginId;
    this.instant.uploadContactList = this.uploadContactList;
    this.postService.saveScheduleMessage(this.instant).map((response) => response)
      .subscribe(data => { 
        this.messageDetail = data.json();
        this.scheduleDate=this.messageDetail[0].scheduleDate;
        this.message=this.messageDetail[0].smsText;
        this.scheduleType=this.messageDetail[0].scheduleType;
        this.numberOfData = this.messageDetail.length;
        this.limit = this.messageDetail.length;
          this.spinner.hide();
      }, error => {
        this.spinner.hide();
        });
  }

  
  setDateFormat(value){
    if(value==undefined || value=="" || value==null){
        value=new Date();
        this.strDate=""+value.getFullYear() + "-" +(value.getMonth()+1)+"-"+value.getDate()+" "+value.getHours()+":"+value.getMinutes()+":"+value.getSeconds()+"";
    }else{
        this.strDate=""+value.getFullYear() + "-" +(value.getMonth()+1)+"-"+value.getDate()+" "+value.getHours()+":"+value.getMinutes()+":"+value.getSeconds()+"";
    }

  }


  open(phone, deleteModal) {
    this.phone = phone;
    this.myModel = this.modelService.open(deleteModal, { windowClass: 'custom-class ' });
  }

  closeModel() {
    this.myModel.close();
  }


  removeMessageDetail(phone) {
    this.spinner.show();
    this.getService.removeMessageDetail(phone.id,this.loginId.loginId)
     .subscribe(data => {
           setTimeout(() => {
       this.closeModel();
      }, 500);
      setTimeout(() =>{
        this.spinner.hide();
      },2000);
        setTimeout(() => {
        this.deleteField = true;
        this.authenticationError = false;
      }, 3000);
      this.messageDetail=data;
      if(this.messageDetail.length<1){
        this.router.navigateByUrl('/sms/message/instant/message');
      }
      setTimeout(() => {
        this.closeModel();
        this.deleteField = false;
      }, 6000);

     },
       error => {
         this.spinner.hide();
         setTimeout(() => {
           this.closeModel();
           this.deleteField = false;
           this.authenticationError = true;

         }, 2000);
         setTimeout(() => {
           this.authenticationError = false;
         }, 4000);
       });
      }
  saveScheduleMessage(value){
   
    this.spinner.show();
    this.getService.scheduleMessageSave(this.loginId,value)
    .subscribe(data=>{
    
     if (data.status == 200) {
       if(value==1)
       {
        this.successField = true;
        this.authenticationError2 = false;
        this.spinner.hide();
       }else{
        this.cancleField = true;
        this.spinner.hide();
       }
      
      
       this.messageDetail=[];
       this.smsDetail={};
       this.authenticationError2 = false;
    }
    
    setTimeout(() => {
      this.successField = false;
      this.cancleField=false;
      this.router.navigate(['/sms/message/instant/message']);
    }, 1000);
   
   
  }, error => {
this.spinner.hide();
    setTimeout(() => {
      this.successField = false;
      this.authenticationError2 = true;
    }, 2000);
    setTimeout(() => {
      this.authenticationError2 = false;
    }, 4000);
  });
}
  


}