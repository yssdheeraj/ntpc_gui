import { Component, OnInit } from '@angular/core';
import {RouterModule,Router} from '@angular/router';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import { User } from '../shared/user';
import { PostService } from '../services/post.service';
import { GetService } from '../services/get.service';
import { ItemDataServiceService } from '../services/item-data-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router:Router,
    private postService: PostService,
    private getService: GetService,
    private itmService: ItemDataServiceService) {}
  
    
    private user: any = {};
    public loginId:any={};
    public userName:any;
    public loginfailDetails: any = [];
    public loginsuccessDetails: any = [];
    public userId:any;
    public lastloginSuccessDetail:any={};
    public lastloginFailDetail:any={};
    
    statusCode: number;

  ngOnInit() {
    this.loginId=this.itmService.getUser();
   // //console.log("user  home ---name ",this.loginId)
    
    this.loginSuccessCount();
    this.loginFailueCount();
    this.lastloginSuccess();
    this.lastloginFail();
   
 
  }
  

  loginSuccessCount() {
    this.userId = this.loginId.loginId;
    this.getService.getloginSuccess(this.userId).subscribe(
      data => {
        this.loginsuccessDetails = data;
       // //console.log("----AdminLoginSuccess LoginCount----" ,this.loginsuccessDetails);
      }
    );
  }

  lastloginSuccess() {
    this.userId = this.loginId.loginId;
    this.getService.getlastloginSuccess(this.userId).subscribe(
      data => {
        this.lastloginSuccessDetail = data;
        ////console.log("----AdminLoginSuccess LoginCount----" ,this.lastloginSuccessDetail);
      }
    );
  }

  loginFailueCount() {
    this.userId = this.loginId.loginId;
    this.getService.getloginFail(this.userId).subscribe(
      data => {
        this.loginfailDetails = data;
        ////console.log("----AdminLoginFail LoginCount----" ,this.loginfailDetails);
      }
    );
  }

  lastloginFail() {
    this.userId = this.loginId.loginId;
    this.getService.getlastloginFail(this.userId).subscribe(
      data => {
        this.lastloginFailDetail = data;
       // //console.log("----AdminLoginSuccess LoginCount----" ,this.lastloginFailDetail);
      }
    );
  }
 
}
