import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'datafilter',
    pure: false
})
export class DataFilterPipe implements PipeTransform {
    transform(items: any[], filter: any): any[] {
      if (!items || !filter) {
        return items;
      }
      // filter items array, items which match and return true will be kept, false will be filtered out
      return items.filter((item: any) => this.applyFilter(item, filter));
    }
    
  
    applyFilter(data: any, filter: any): boolean {
      ////console.log(data,"      ",filter)
      for (let field in filter) {
        if (filter[field]) {
          if (typeof filter[field] === 'string') {
            if (data[field].toLowerCase().indexOf(filter[field].toLowerCase()) === -1) {
              return false;
            }
          } else if (typeof filter[field] === 'number') {
            if (data[field] !== filter[field]) {
              return false;
            }
          }
        }
      }
      return true;
    }
  }

