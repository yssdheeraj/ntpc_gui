import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigureKeywordComponent } from './configure-keyword.component';

describe('ConfigureKeywordComponent', () => {
  let component: ConfigureKeywordComponent;
  let fixture: ComponentFixture<ConfigureKeywordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigureKeywordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigureKeywordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
