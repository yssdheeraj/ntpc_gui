import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PullReportComponent } from './pull-report.component';

describe('PullReportComponent', () => {
  let component: PullReportComponent;
  let fixture: ComponentFixture<PullReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PullReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PullReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
