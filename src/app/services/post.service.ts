import { Injectable,Type } from '@angular/core';
import { Http, Response,RequestOptions,Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import{Location} from '../shared/location'
import{Department} from '../shared/department';
import { Options } from 'selenium-webdriver';
import { error } from 'util';
import { errorHandler } from '@angular/platform-browser/src/browser';
import { MapType } from '@angular/compiler/src/output/output_ast';
import { Observer } from 'rxjs/Observer';
import { ItemDataServiceService } from './item-data-service.service';


@Injectable()
export class PostService {
private option:any;
location:Location;
private mylist:string;


 private baseUrl:string = "";

 private smsUrl:string ="";

// private baseUrl = "http://localhost:8080/NTPC_APP";
// private smsUrl = "http://localhost:8080/NTPC_SMS";

// private baseUrl = "http://localhost:8080/NTPC_APP";
//   private smsUrl = "http://localhost:8080/NTPC_SMS";


private headers=new Headers({'content-type':'application/json'});
private options=new RequestOptions({headers:this.headers});
private list = new Map<Type<string>, Observable<any>>();

  constructor(private http: Http,private itemDataServiceService: ItemDataServiceService) { }

  ngOnInit(){
   
  this.getUrl();
  }
  getUrl(){
    this.baseUrl=this.itemDataServiceService.getBaseurl();
    this.smsUrl=this.itemDataServiceService.getSmsurl();

  //  console.log(this.baseUrl,"   post  ",this.smsUrl);
  }

saveLocation(value:any):Observable<any>{
  this.getUrl();
 return this.http.post(this.baseUrl+"/location/createlocation",JSON.stringify(value),this.options);
}

updateLocation(value:any):Observable<any>{
  this.getUrl();
  return this.http.put(this.baseUrl+"/location/updatelocation",JSON.stringify(value),this.options);
 }

 


 saveUser(value:any):Observable<any>{
  this.getUrl();
   return this.http.post(this.baseUrl+"/user/createuser",JSON.stringify(value),this.options);
 }

 updateUser(value:any):Observable<any>{
  this.getUrl();
  return this.http.put(this.baseUrl+"/user/updateuser",JSON.stringify(value),this.options);
}







saveSMS(value:any,uploadFile:any):Observable<any>{
 
  this.getUrl();
  let headers = new Headers();
   headers.append('enctype', 'multipart/form-data');
   let options = new RequestOptions({ headers: headers });
   let formData = new FormData();
   formData.append("smsName",value.smsName);
   formData.append("smsDesc", value.smsDesc);
   formData.append("loginId", value.createdBy.loginId);
   formData.append("uploadFile", uploadFile);
// //console.log("uplode file "+uploadFile);

   
   return this.http.post(this.baseUrl+"/sms/createsms",formData,options);
 }
 

updateSMS(value:any):Observable<any>{
  this.getUrl();
 return this.http.put(this.baseUrl+"/sms/updatesms",JSON.stringify(value),this.options);
}





saveContact(value: any): Observable<any> {
  this.getUrl();
  return this.http.post(this.baseUrl+'/phonedirectory/createPhoneDirectory', JSON.stringify(value),this.options);
}


updateContact(value: any){
  this.getUrl();
 return this.http.put(this.baseUrl+"/phonedirectory/updatePhoneDirectory",JSON.stringify(value),this.options);
}


createGroup(value: any){
  this.getUrl();
   return this.http.post(this.baseUrl+'/contactgroup/createGroup', JSON.stringify(value),this.options);
 }

 updateGroup(value:any) :Observable<any>{
  this.getUrl();
  return this.http.put(this.baseUrl +'/contactgroup/updateGroup', JSON.stringify(value),this.options);
 }
 
addContactinGroup(value:any){
 //debugger;
 this.getUrl();
  return this.http.post(this.baseUrl+'/allcontact/addContactGrp', JSON.stringify(value),this.options);
}



getOneGroup(value:any):Observable<any>{
  this.getUrl();
  return this.http.post(this.baseUrl+'/ongroupcontact',JSON.stringify(value),this.options);
//  return this.http.post(this.baseUrl + "/ongroupcontact/" +value).map(success => success.status)
}


//save Department
saveDepartment(value:any) :Observable<any>{
  this.getUrl();
  return this.http.post(this.baseUrl + "/department/department/create" ,JSON.stringify(value) ,this.options);
}


//update Department
updateDepartment(value:any) :Observable<any>{
  this.getUrl();
  return this.http.put(this.baseUrl + "/department/department/update" ,JSON.stringify(value) ,this.options);
}

//login
doLogin(value:any) :Observable<any>{
  this.getUrl();
  return this.http.post(this.baseUrl + "/index/user/login" , JSON.stringify(value),this.options);
}

doLogout(value:any) :Observable<any>{
  this.getUrl();
  return this.http.post(this.baseUrl + "/index/user/logout" , JSON.stringify(value),this.options);
}

// saveFile(value:any):Observable<any>{
//   return this.http.post(this.baseUrl+"/createFileUpload",JSON.stringify(value),this.options);
// }

saveFile(value:any):Observable<any>{
  this.getUrl();
  let headers = new Headers();
  headers.append('enctype', 'multipart/form-data');
  let options = new RequestOptions({ headers: headers });
  let formData = new FormData();
  formData.append("txtFile", value);
  //formData.append("jobId", jobId);
  
  ////console.log(formData)
    return this.http.post(this.baseUrl+"/fileupload/createFileUpload",formData,options);
  }
  
changeAdminPassword(value:any):Observable<any>{
  this.getUrl();
  return this.http.post(this.baseUrl + "/index/password/reset" ,JSON.stringify(value),this.options);
}

changeUserPassword(value:any):Observable<any>{
  this.getUrl();
    return this.http.post(this.baseUrl + "/index/password/change" ,JSON.stringify(value),this.options);
  }

saveInstaceMessage(value:any):Observable<any>{
  this.getUrl();
  return this.http.post(this.baseUrl+'/intancemessage/savemessageforsms',JSON.stringify(value),this.options);
}





saveScheduleMessage(value:any):Observable<any>{

  this.getUrl();
  return this.http.post(this.baseUrl+'/sms/savetempmessage',JSON.stringify(value),this.options);
}

applyFilter(value :any) : Observable<any> {
  this.getUrl();
  return this.http.post(this.baseUrl + '/report/report/filter' ,JSON.stringify(value),this.options);
  }
  
  

uploadTextDocument(docName,loginId,messageType){
  this.getUrl();
  let headers = new Headers();
  headers.append('enctype' , 'multipart/form-data');
  let options = new RequestOptions({ headers : headers});
  let formData = new FormData();
  formData.append("uploadTextDocument", docName);
  ////console.log("####### uploadTextDocument #########" , docName);
  formData.append("loginId" , loginId);
  formData.append("messageType",messageType)
  ////console.log("####### loginId #########" , loginId);
  return this.http.post(this.baseUrl + '/fileupload/upload/textFileDocument', formData,options);
}
uploadContactNumber(docName,loginId){
  this.getUrl();
   let headers = new Headers();
   headers.append('enctype' , 'multipart/form-data');
   let options = new RequestOptions({ headers : headers});
   let formData = new FormData();
   formData.append("contactFile", docName);
   ////console.log("####### uploadTextDocument #########" , docName);
   formData.append("loginId" , loginId);
    ////console.log("####### loginId #########" , loginId);
   return this.http.post(this.baseUrl + '/sms/readcontacatfile', formData,options);
 }

// forgetPasword(loginId,mobile){
//   debugger
//   let headers = new Headers();
//   headers.append('enctype','multipart/form-data');
//   let options = new RequestOptions({headers : headers});
//   let formData = new FormData();
//   formData.append("loginId",loginId);
//   formData.append("mobile",mobile);
//   return this.http.post(this.baseUrl + '/forget/password', formData,options);
// }

forgetPasword(value:any):Observable<any>{
  this.getUrl();
  return this.http.post(this.baseUrl + "/index/forget/password", JSON.stringify(value),this.options);
}

//save mail configuration
saveSmtpMailConfiguration(value:any) :Observable<any>{
  this.getUrl();
  return this.http.post(this.baseUrl + "/saveconfig" ,JSON.stringify(value) ,this.options);
}

//update  mail configuration
updatSmtpMailConfiguration(value:any) :Observable<any>{
  this.getUrl();
  return this.http.put(this.baseUrl + "/updateconfig" ,JSON.stringify(value) ,this.options);
}

startURLService(value:any) :Observable<any>{
  this.getUrl();
  return this.http.post(this.smsUrl + "/start/smsSender/service" ,JSON.stringify(value) , this.options)
}


stopURLService(value:any) :Observable<any>{
  this.getUrl();
  return this.http.post(this.smsUrl + "/stop/smsSender/service" ,JSON.stringify(value) , this.options)
}

updatesmsUrl(value:any):Observable<any>{
  this.getUrl();
  return this.http.put(this.baseUrl + "/user/saveUpdate/url" , JSON.stringify(value), this.options)
}

saveLatestNews(value:any):Observable<any>{
  this.getUrl();
  return this.http.post(this.baseUrl + "/user/save/news" ,JSON.stringify(value), this.options);
}

// startURLService(value:any):Observable<any[]>{
//   
//   return this.http.get(this.smsUrl + "/start/smsSender/service").map(res => res.text() ? res.json() : res);
// }0

// stopURLService(value:any):Observable<any[]>{
//     
//   return this.http.get(this.smsUrl + "/stop/smsSender/service").map(res => res.text() ? res.json() : res);
// }

updateNews(value:any):Observable<any>{
  this.getUrl();
  return this.http.put(this.baseUrl + "/user/update/news" ,JSON.stringify(value),this.options);
}

}
