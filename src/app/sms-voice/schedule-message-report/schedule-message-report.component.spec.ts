import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleMessageReportComponent } from './schedule-message-report.component';

describe('ScheduleMessageReportComponent', () => {
  let component: ScheduleMessageReportComponent;
  let fixture: ComponentFixture<ScheduleMessageReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleMessageReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleMessageReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
