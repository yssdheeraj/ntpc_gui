import { Component, OnInit } from '@angular/core';
import { GetService } from '../../services//get.service';
import { PostService } from '../../services/post.service';
import { Observable } from 'rxjs/Observable';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemDataServiceService } from '../../services/item-data-service.service';

@Component({
  selector: 'app-instant-voice',
  templateUrl: './instant-voice.component.html',
  styleUrls: ['./instant-voice.component.css']
})
export class InstantVoiceComponent implements OnInit {

 

  public phoneDirctoryList: any[];
  public contactGroupList: any[];
  public departmentList: any[];
  public duplicatList: any = [];
  public contactGroup: any = "contactGroup";
  public department: any = "departments";
  public phoneDirectory: any = "phoneDirectory";
  public message_: any = "message";
  public messageName_: any = "messageName";
  public mobile_: any = "mobile";
  public loginId: any = {};
  public phoneDirectorys: any[];
  public contactGroups: any[];
  public departments: any[];
  public mobile: any;
  public messageName: any;
  public message: any;
  public successField: any = false;
  public successEdit: boolean;
  public authenticationError: boolean;
  public deleteField: boolean;
  public chooseOne: boolean;
  public requiredMsg: string;
  public messageSelect: boolean;
  public required: string;
  public errorborder:any;
  public scheduleType: any = "ONETIME";
  public smsPriority:any="MEDIUM";
 
  
  public scheduleTypes: any = [

    { id: 0, scheduleType: "ONETIME", value: "ONETIME" },
    { id: 1, scheduleType: "DAILY", value: "DAILY" },
    { id: 2, scheduleType: "WEEKLY", value: "WEEKLY" },
    { id: 3, scheduleType: "MONTHLY", value: "MONTHLY" },
    { id: 4, scheduleType: "YEARLY", value: "YEARLY" }
  ]

  public smsPriorities: any = [

    
    { id: 0, priority: "MEDIUM", value: "MEDIUM" },
    { id: 1, priority: "LOW", value: "LOW" },
    { id: 2, priority: "HIGH", value: "HIGH" }
    
  ]



  public count1: any = 0;
  public sms: any = [{
    phoneDirectorys: [],
    contactGroups: [],
    departments: [],
    mobile: '',
    messageName: '',
    message: '',
    priority: '',
    scheduleDate: '',
    scheduleType: '',
    loginId:{}


  }];

  public smsList: any[];
  public noContactCreated: boolean = false;
  constructor(private itemDataServiceService: ItemDataServiceService, private getService: GetService, private postService: PostService, private spinner: NgxSpinnerService) { }
  public items: any[];
  ngOnInit() {

    this.getPhoneDircatoryList();
    this.getContactGrouplist();
    this.getSmsList();
    this.getAlldepartment();
    this.loginId = this.itemDataServiceService.getUser();

  }


  getPhoneDircatoryList() {

    this.getService.getContactList().subscribe(data => {
      this.phoneDirctoryList = data;
      this.noContactCreated = false;

      //console.log(data)
      //console.log("phoneDirctoryList :" + this.phoneDirctoryList);

      if (this.phoneDirctoryList.length < 1) {
        this.noContactCreated = true;
      }
    },
      error => {
        // this.alertService.error(error);
        //    this.loading = false;
      });
  }


  getContactGrouplist() {
    this.getService.getGrouplist().subscribe(data => {
      this.contactGroupList = data;
      //console.log(data)
      //console.log("contactGroupList :" + this.contactGroupList);

    },
      error => {
        //console.log(error);
      });
  }

  getSmsList() {
   
    this.getService.getVoiceSmsList().subscribe((data) => {
      this.smsList = data;
      //console.log(this.smsList)
      //console.log(" voice sms list" , data);
    }, (error) => {
      //console.log(error);
    });
  }

  getAlldepartment() {
    this.getService.getDepartmentList().subscribe(
      data => {
        this.departmentList = data;
        //console.log("Fetch All department :  ", data);
      },
      errorCode => {

        //console.log(errorCode);
      }
    );
  }

  saveInstaceVoiceMessage(){
    alert("Hello !!")

    
    this.chooseOne = false;
    if (this.sms.phoneDirectory == undefined && this.sms.contactGroup == undefined && this.sms.departments == undefined && this.sms.mobile == undefined) {

      this.chooseOne = true;
      this.requiredMsg = "Please Choose one of these";
      this.errorborder='1px solid red';
     
    }
   else{
    this.chooseOne = false;
    this.messageSelect = false;
   
    if (this.sms.messageName == undefined && this.sms.message == undefined) {
     
      this.messageSelect = true;
      this.requiredMsg = "Required"
      this.errorborder='1px solid red';
    } else{
    this.chooseOne = false;
    this.messageSelect = false;
   
//    }
//    }
    
    

// if(!this.chooseOne && !this.messageSelect)
// {
    
    this.spinner.show();

    //console.log(this.scheduleType);
    //console.log(this.smsPriority);
    //console.log(this.sms);
    // this.saveSmsList.set(this.contactGroup,this.sms.contactGroup);
    // this.saveSmsList.set(this.department,this.sms.departments);
    // this.saveSmsList.set(this.phoneDirectory,this.sms.phoneDirectory);
    // this.saveSmsList.set(this.message_,this.sms.message);
    // this.saveSmsList.set(this.messageName_,this.sms.messageName);
    // this.saveSmsList.set(this.mobile_,this.sms.mobile)
    // //console.log("all data ---- ",this.saveSmsList)

    // this.postService.saveInstaceMessage(this.saveSmsList).map((response)=>response)
    // .subscribe(
    //   data=>{
    //       //console.log("save instant message",data);
    //   }
    // );

    // this.instant.mobile = this.sms.mobile;
    // this.instant.smsName = this.sms.messageName;
    // this.instant.smsText = this.sms.message;
    // this.instant.phoneDirectoryName = this.sms.phoneDirectory;
    // this.instant.contactGroupName = this.sms.contactGroup;
    // this.instant.departmentName = this.sms.departments;
    // this.instant.priority = this.smsPriority;
    // this.instant.scheduleDate = this.sms.scheduleDate;
    this.sms.scheduleType = this.scheduleType;
    this.sms.loginId = this.loginId;


    this.postService.saveScheduleMessage(this.sms).map((response) => response)

      .subscribe(data => {
        //console.log("save instant message", data);

        this.duplicatList = data.json();
        //console.log("ddddd   ", this.duplicatList)
        //console.log(data);
        if (data.status == 200) {
          setTimeout(() => {
            this.spinner.hide();
            this.successField = true;
            this.authenticationError = false;
          }, 3000);

        }
        setTimeout(() => {
          this.successField = false;
        }, 6000);

      },

        error => {


          setTimeout(() => {
            this.spinner.hide();
            this.successField = false;
            this.authenticationError = true;

          }, 2000);


          setTimeout(() => {
            this.authenticationError = false;
          }, 6000);

        });
      }
    }
  

  }
  checkRequired()
  {
    this.chooseOne = false;
    if (this.sms.phoneDirectory == undefined && this.sms.contactGroup == undefined && this.sms.departments == undefined && this.sms.mobile == undefined) {

      this.chooseOne = true;
      this.requiredMsg = "Please Choose one of these";
      
     
    }
   else{
    this.chooseOne = false;
    this.messageSelect = false;
    
    if (this.sms.messageName == undefined && this.sms.message == undefined) {
     
      this.messageSelect = true;
      this.requiredMsg = "Required"
     
    } else{
    this.chooseOne = false;
    this.messageSelect = false;
   
  }
}

  }}
