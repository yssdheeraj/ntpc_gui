import { Component, OnInit, group, ViewChild } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { GetService } from '../../services//get.service';
import { PostService } from '../../services/post.service';
import { ItemDataServiceService } from '../../services/item-data-service.service';
import { NgbModal,ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { AppComponent } from '../../app.component';



 



@Component({
  selector: 'app-sms-voice-draft',
  templateUrl: './sms-voice-draft.component.html',
  styleUrls: ['./sms-voice-draft.component.css']
})
export class SmsVoiceDraftComponent implements OnInit {

  @ViewChild('smsForm') form: any;
  public sms: any = {};
  public smsDraft: any = {};
  public smsList: any = [];
  public smsName: any;
  public smsDesc: any;
  public fileName: any;
  public uploadFile: File = null;
  public location: any = {};
  public locationList: any[];
  myModel: any;
  public noSMSCreated: boolean;
  public loginId: any = {};
  public AllMessage: boolean = false;
  public successField: any = false;
  public successEdit: boolean;
  public authenticationError: boolean;
  public deleteField: boolean;

  name = 'Angular v4 - Applying filters to *ngFor using pipes';
  numberOfData: number;
  limit: number;
  page: number = 1;
  filter: any = {};

  closeResult: string;

  constructor(private getService: GetService, private modelService: NgbModal, private postService: PostService,
    private itemDataServiceService: ItemDataServiceService, private spinner: NgxSpinnerService,private app:AppComponent) { }

  ngOnInit() {
    this.loginId = this.itemDataServiceService.getUser();
    this.getSmsList();
  }


  getLocationlist() {
    this.getService.getLocationList().subscribe((locationList) => {
      this.locationList = locationList;
      ////console.log(locationList);

    }, (error) => {
      ////console.log(error);
    }
    );
  }


  getSmsList() {
    this.app.checkCredential();
    this.getService.getSmsList().subscribe((data) => {
      this.smsList = data;
      this.numberOfData = this.smsList.length;
      // this.limit = this.smsList.length;
      this.noSMSCreated = false;
      ////console.log("sms list",data);
      if (this.smsList < 1) {
        this.noSMSCreated = true;
      }
      else {
        this.numberOfData = this.smsList.length;
        this.limit = this.smsList.length;
        this.noSMSCreated = false;
      }
    }, (error) => {
      ////console.log(error);
    });
  }


  onSubmit() {

    this.spinner.show();
    this.loginId = this.itemDataServiceService.getUser();
    this.sms.createdBy = this.loginId;
    this.sms.createDate = new Date();
    //  this.location.locationId=this.sms.location;
    //  this.location.locationName=this.sms.location.locationName;
    //  this.sms.location=this.location;
    //    ////console.log("locat i"+this.location);



    //   ////console.log(" sms =>"+this.sms.toString());
    ////console.log(this.fileName);

    this.postService.saveSMS(this.sms, this.uploadFile).map((response) => response)
      .subscribe(data => {
        this.getSmsList();
        if (this.form.valid) {
          ////console.log("Form Submitted!",data);
          this.form.reset();
        }

        if (data.status == 201) {

          this.spinner.hide();
          this.successField = true;
          this.authenticationError = false;



        }
        setTimeout(() => {
          this.successField = false;
        }, 2000);

      },

        error => {


          setTimeout(() => {
            this.spinner.hide();
            this.successField = false;
            this.authenticationError = true;

          }, 2000);


          setTimeout(() => {
            this.authenticationError = false;
          }, 4000);

        });


  }

  updateSms() {
    this.spinner.show();
    this.sms.createDate = new Date();
    this.postService.updateSMS(this.smsDraft).map((response) => response)
      .subscribe(data => {
        this.getSmsList();
        ////console.log("update data"+data);
        if (data.status == 201) {

          this.spinner.hide();
          this.successEdit = true;
          this.authenticationError = false;

        }
        setTimeout(() => {
          this.successEdit = false;
          this.closeModel();
        }, 2000);
      },
        error => {

          setTimeout(() => {
            this.spinner.hide();
            this.successEdit = false;
            this.authenticationError = true;
          }, 2000);

          setTimeout(() => {
            this.authenticationError = false;
            this.closeModel();
          }, 4000);

        });
  }

  // open(sms,deleteModal) {
  //   debugger
  //   this.modelService.open(sms).result.then((result) => {
  //     this.closeResult = `Closed with: ${result}`;
  //   }, (reason) => {
  //     this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  //   });
  // }

  // private getDismissReason(reason: any): string {
  //   if (reason === ModalDismissReasons.ESC) {
  //     return 'by pressing ESC';
  //   } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
  //     return 'by clicking on a backdrop';
  //   } else {
  //     return `with: ${reason}`;
  //   }
  // }

  open(sms,deleteModal) {
    
     this.sms = sms;
    ////console.log("***********************" ,sms);
      this.myModel = this.modelService.open(deleteModal ,  { windowClass: 'custom-class ' });
  }
  // closeModel() {
  //   this.departmentExists=false;
  //   this.getAlldepartment();
  //   this.myModel.close();
  //   this.department = this.empty;
  //   ////console.log(this.department);

  // }

  deleteSms(sms) {
    //debugger
   // this.spinner.show();
    this.deleteField = false;
    //this.sms=sms;
    this.getService.deleteSMS(sms.id).map((response) => response)
      .subscribe(data => {
        setTimeout(() => {
          this.closeModel();
          this.sms.smsName='';
          this.sms.smsDesc='';
        }, 500);
        ////console.log(data);
        //this.deleteField = true;
        setTimeout(() => {
        //  this.spinner.hide();
          this.closeModel();
          this.deleteField = true;
          this.authenticationError = false;
          
        }, 500);

        this.getSmsList();
        setTimeout(() => {
          this.deleteField = false;
        }, 2000);

      },
        error => {
          setTimeout(() => {
          //  this.spinner.hide();
            this.closeModel();
            this.deleteField = false;
            this.authenticationError = true;

          }, 2000);


          setTimeout(() => {
            this.authenticationError = false;
          }, 2000);
        });
  }
  OpenModel(smsDraft, updateModal) {
    this.smsDraft = smsDraft;

    this.myModel = this.modelService.open(updateModal);

  }

  closeModel() {
    this.getSmsList();
    this.myModel.close();



  }
  viewSmsList() {
    this.AllMessage = false;
  }
  addSmsTemplate() {
    this.AllMessage = true;
  }


  onTextFileChange(event) {

    let fileList: FileList = event.target.files;

    if (fileList.length > 0) {

      if (fileList && event.target.files[0]) {
        let file: File = fileList[0];

        var urlSt = window.URL.createObjectURL(file);
        //  var urlSt = URL.createObjectURL(fileList);
        ////console.log("url...",urlSt);
        var audio = new Audio(urlSt);
        ////console.log(urlSt)
        ////console.log("my upload file ",urlSt)
        if (file.type == "audio/wav") {
          ////console.log(file.type);
          // alert("this is text file");
          ////console.log(file)
          this.uploadFile = file;
          alert("file successfully upload")

          // //console.info('onAfterAddingFile', fileItem);
          //   var urlSt = URL.createObjectURL(fileItem._file);
          //   var audio = new Audio(urlSt);

        }
        else {
          alert("please fill right file");

        }

      }

    }
  }

}
