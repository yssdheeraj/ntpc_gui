import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantMessageReportComponent } from './instant-message-report.component';

describe('InstantMessageReportComponent', () => {
  let component: InstantMessageReportComponent;
  let fixture: ComponentFixture<InstantMessageReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantMessageReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantMessageReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
