import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageTrunkReportComponent } from './message-trunk-report.component';

describe('MessageTrunkReportComponent', () => {
  let component: MessageTrunkReportComponent;
  let fixture: ComponentFixture<MessageTrunkReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageTrunkReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageTrunkReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
