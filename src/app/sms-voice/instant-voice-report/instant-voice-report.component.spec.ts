import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantVoiceReportComponent } from './instant-voice-report.component';

describe('InstantVoiceReportComponent', () => {
  let component: InstantVoiceReportComponent;
  let fixture: ComponentFixture<InstantVoiceReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantVoiceReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantVoiceReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
